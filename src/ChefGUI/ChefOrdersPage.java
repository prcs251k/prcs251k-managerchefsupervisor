/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ChefGUI;
//An ArayList which contains OrderDetails objects
import DatabaseConnection.JavaConnectDb;
import LoginGUI.Login;
import StaffGUI.StaffOrdersPage;
import java.awt.Color;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.lang.String;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import static java.util.Collections.list;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import oracle.net.aso.i;
import restaurant.staffdatamodel.Item;
import restaurant.staffdatamodel.OrderDetails;

/**
 *
 * @author Panagiotis
 */
public class ChefOrdersPage extends javax.swing.JFrame {
    

    //The number of orders running at the moment. (like COUNT)
    private final int numberOfOrders =  InitialiseNumberOfOrders();
    //The total number of all ITEM_IDs in ORDER_DETAIL. (like COUNT)
    private final int numberOfItems = InitialiseNumberOfItems();

    //Contains the ORDER_IDs from ORDER_DETAIL only once (DISTINCT).
    private String[] arrayOrdersId = new String[numberOfOrders];
    //Contains the ITEM_IDs (NOT DISTINCT).
    private String[] arrayItemsNames = new String[numberOfItems];
    //Contains the SIZE_IDs only once (DISTINCT).
    private String[] arraySizeId = new String[numberOfOrders];
    
    
    
    

    //Contains All the jLists from GUI
    private JList[] arrayJlists = new JList[10]; 
    
    //Contains All the jLabels from GUI
    private JLabel[] arrayJlabels = new JLabel[10];
    
    //Contains All the jButtonsTake from GUI
    private JButton[] arrayJbuttonsTake = new JButton[10];
    
    //Contains All the jButtonsTick from GUI
    private JButton[] arrayJbuttonsTick = new JButton[10];

    
    
    //Contains ListModels as many as the jLists are on the gui.
    private DefaultListModel[] arrayDeafaultListModel = new DefaultListModel[10];
    DefaultListModel list = new DefaultListModel();
    
    //Used to get an ORDER_ID and allocate the items that correspond to it.
    ArrayList<String> orderIdList = new ArrayList<String>();        
    //Contains the Items of an order.
    ArrayList<String[]> itemNameList = new ArrayList<String[]>();
    

    //Contain the sizes from the ORDER_DETAIL table. 
    ArrayList<String> itemSizeList = new ArrayList<String>();
    //Contain the quantities from the ORDER_DETAIL table. 
    ArrayList<Integer> itemQuantityList = new ArrayList<Integer>();
    
    
    /**
     * Initialises the GUI.
     */
    public ChefOrdersPage() 
    {
        initComponents();
        InitialiseInstances();
        
        this.setExtendedState(this.MAXIMIZED_BOTH);
        
        
        
        for(int i = 0; i < arrayOrdersId.length; i++)
        {
            ShowOrdersInJlists(i);
           
        }

    }

  
    
    /**
     * Counts how many different ORDER_ID exist in the ORDER_DETAIL.
     * @return the number of different ORDER_IDs.
     */
    public int InitialiseNumberOfOrders() 
    {
        int numberOfOrders = 0;
        
        ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
                
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT DISTINCT ORDER_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {            
                orderDetails = new OrderDetails(rs.getString("ORDER_ID"));
                orderIdList.add(orderDetails);   
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        numberOfOrders = orderIdList.size();
        return numberOfOrders;
    }
    
    
    /**
     * Counts how many ITEM_IDs exist in the ORDER_DETAIL.
     * @return the number of Items in ORDER_DETAIL table.
     */
    public int InitialiseNumberOfItems()
    {
        int numberOfItems = 0;
        
         ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT ITEM_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {
                orderDetails = new OrderDetails(rs.getString("ITEM_ID"));
                orderIdList.add(orderDetails);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        numberOfItems = orderIdList.size();
        return numberOfItems;
    }
    
      
    
    /**
     * Puts the ORDER_IDs in an array.
     * @return An array with all the ORDER_IDs in ORDER_DETAIL table.
     */
    public String[] InitialiseContentOfArrayOrdersId() 
    {
        String[] arrayContentOfArrayOrdersId = new String[numberOfOrders];
        
        ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        int i = 0;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT DISTINCT ORDER_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {
                orderDetails = new OrderDetails(rs.getString("ORDER_ID"));
                orderIdList.add(orderDetails);                
                arrayContentOfArrayOrdersId[i] = orderDetails.getOrderId();   
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
      return arrayContentOfArrayOrdersId;
    }
    
    
    /**
     * Puts the ITEM_IDs in an array
     * @return An array with all the ITEM_IDs in ORDER_DETAIL table.
     */
    public String[] InitialiseContentOfArrayItemsNames() 
    {
        String[] arrayContentOfArrayItemsId = new String[numberOfItems];
        
        ArrayList<Item> itemIdList = new ArrayList<Item>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        int i = 0;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL");
            Item item;
            while(rs.next())
            {
                item = new Item(rs.getString("ITEM_NAME"));
                itemIdList.add(item);                
                arrayContentOfArrayItemsId[i] = item.getItemName();
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
      return arrayContentOfArrayItemsId;
    }
    
    
    /**
     * Puts the SIZE_IDs in an array
     * @return An array with all the SIZE_IDs in ORDER_DETAIL table.
     */
    public String[] InitialiseContentOfArraySizeId()
    {
        String[] arrayContentOfArraySizeId = new String[numberOfItems];
        
        ArrayList<Item> itemIdList = new ArrayList<Item>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        int i = 0;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT SIZE_ID FROM ORDER_DETAIL");
            Item item;
            while(rs.next())
            {
                item = new Item(rs.getString("SIZE_ID"));
                itemIdList.add(item);                
                arrayContentOfArraySizeId[i] = item.getItemName();
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
      return arrayContentOfArraySizeId;
    }
    
    
    
    
    
 
    /**
     * THIS FUNCTION CONTAINS A BUG. IT DOES NOT DISPLAY THE CORRECT QUANTITY AND SIZE VALUES FOR THE ITEMS.
     * The 1st element of orderIdList corresponds on the 1st element of itemNameList the 2nd on the 2nd and so on..
     * By following this rule this method allocates the corresponding Items to an OrderId.
     * This function provides only the food.
     * @param rowIndex is the index of orderIdList and itemNameList in every repetition.
     */
    public void AllocateItemsToOrderId(int rowIndex)
    {
        Connection conn = JavaConnectDb.ConnectDb();        
        
              
        String[] itemName = new String[numberOfItems]; //Auto prepei na allazei to length tou analoga me to posa items exei kathe paraggelia (Thelei sql COUNT). --> new String[numberOfItemsInAnOrder]; 
        
        
        Statement st;
        ResultSet rs;
        int i = 0;       
        try
        {
            st = conn.createStatement();
            
            //Store PIZZA
            rs = st.executeQuery("SELECT ITEM_NAME, ITEM_SIZE, QUANTITY FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'PIZZA'");          
                     
            orderIdList.add(arrayOrdersId[rowIndex]);
            
           
            while(rs.next())
            {
                itemName[i] = rs.getString("ITEM_NAME");
                AllocateSizeToItem(rowIndex);
                AllocateQuantityToItem(rowIndex);
                
                
                itemName[i] = itemQuantityList.get(rowIndex)+ "x" + "  "  + itemName[i] + " "+itemSizeList.get(rowIndex); //Make a method that returns the size  itemName[i] = Methodquantity.Tostring() + itemName[i] + AllocateSizeToItem(i)
                
                i++;
                itemSizeList.remove(rowIndex);
                itemQuantityList.remove(rowIndex);
            }     
            
        
            //Store SIDES
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'SIDE'");          
            while(rs.next())
            {
                itemName[i] = rs.getString("ITEM_NAME");
                AllocateSizeToItem(rowIndex);
                AllocateQuantityToItem(rowIndex);
                
                
                itemName[i] = itemQuantityList.get(rowIndex)+ "x" + "  "  + itemName[i] + " " +itemSizeList.get(rowIndex); //Make a method that returns the size  itemName[i] = Methodquantity.Tostring() + itemName[i] + AllocateSizeToItem(i)
                
                i++;
                itemSizeList.remove(rowIndex);
                itemQuantityList.remove(rowIndex);

            }   
            
            
            //Store DESERT
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'DESERT'");          
            while(rs.next())
            {
                itemName[i] = rs.getString("ITEM_NAME");
                AllocateSizeToItem(rowIndex);
                AllocateQuantityToItem(rowIndex);
                
                
                itemName[i] = itemQuantityList.get(rowIndex)+ "x" + "  "  + itemName[i] + " " +itemSizeList.get(rowIndex); //Make a method that returns the size  itemName[i] = Methodquantity.Tostring() + itemName[i] + AllocateSizeToItem(i)
                
                i++;
                itemSizeList.remove(rowIndex);
                itemQuantityList.remove(rowIndex);
            }   
            
            

            itemNameList.add(itemName);
            
            
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    /**
     * Allocate size to Items in an orderId.
     * @param rowIndex is the OrderId where it gets the size for.
     */
    public void AllocateSizeToItem(int rowIndex)
    {
        Connection conn = JavaConnectDb.ConnectDb();        
        
              
        String[] itemSize = new String[numberOfItems]; //Auto prepei na allazei to length tou analoga me to posa items exei kathe paraggelia (Thelei sql COUNT). --> new String[numberOfItemsInAnOrder]; 
         
        
        Statement st;
        ResultSet rs;
        int i = 0;       
        try
        {
            st = conn.createStatement();
            
            rs = st.executeQuery("SELECT ITEM_SIZE FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex]);
            
            while(rs.next())
            {
                itemSize[i] = rs.getString("ITEM_SIZE");
                itemSizeList.add(itemSize[i]);
            }
    
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    
    
    
    /**
     * Allocate quantity of an Item in an orderId.
     * @param rowIndex is an OrderId where it gets the quantity for.
     */
    public void AllocateQuantityToItem(int rowIndex)
    {
        Connection conn = JavaConnectDb.ConnectDb();  
        Integer[] itemQuantity = new Integer[numberOfItems];

        
        Statement st;
        ResultSet rs;
        int i = 0;       
        try
        {
            st = conn.createStatement();
            
            rs = st.executeQuery("SELECT QUANTITY FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex]);
            
            while(rs.next())
            {
                itemQuantity[i] = rs.getInt("QUANTITY");
                itemQuantityList.add(itemQuantity[i]);
                
            }
    
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
    
    
    
    
  
    /**
     * THIS FUNCTION DOES NOT WORK PROPERLY
     * Hold all the JLists in an array.
     *  Check if any of these are empty(null).
     * If yes fill it with any available order.
     * @param index is the index of the arrays and ArrayLists used.
     */
    public void ShowOrdersInJlists(int index)
    {
        arrayJlabels[index].setText("ORDER ID: " + orderIdList.get(index));
        arrayJlists[index].setModel(arrayDeafaultListModel[index]);
        
        
        String[] arrayItemsInOrder = new String[numberOfItems];    
        arrayItemsInOrder = itemNameList.get(index);
                     
        int numberOfItemsInOrder = arrayItemsInOrder.length;
        
        
        //Display the items in rows
        for(int i = 0; i < numberOfItemsInOrder; i++) 
        {     
            arrayDeafaultListModel[index].addElement(arrayItemsInOrder[i]);
        }
                
    }
    
    

    
    
    
    /**
     * When a Take button in GUI gets clicked this means that the food is getting prepared.
     * This function changes the ORDER_STATUS to "BEING PREPARED".
     * @param YesNo is the Decision made by the user on YES_NO confirmation.
     * @param index is the index of the arrays and ArrayLists used.
     */
    public void ChangeOrderStatusOnTake(int YesNo,int index)
    {
        Connection conn = JavaConnectDb.ConnectDb(); 
        Statement st;
        ResultSet rs;
        
        
        
        if(YesNo == 0) //0 means YES 
        {
            arrayJbuttonsTake[index].setText("Order Taken -\nTick when finish");
            arrayJbuttonsTake[index].setBackground(Color.red);
            arrayJbuttonsTick[index].setEnabled(true);
            
            
            //Prepei na kanw kai clear ta elements twn arrayLists orderIdList kai itemNameList gia na mpei kapoia allh paraggelia efoson auth eiani etoimh.
            
            try
            {
               
                PreparedStatement ps = conn.prepareStatement("UPDATE MAIN_ORDER SET ORDER_STATUS = ? WHERE ORDER_ID = ? ");
                ps.setString(1, "BEING PREPARED");
                ps.setString(2, arrayOrdersId[index]);
                
                ps.executeUpdate();
                ps.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            } 
        }
        else
        {
            arrayJbuttonsTake[index].setText("TAKE ORDER");
            arrayJbuttonsTake[index].setBackground(Color.GREEN);
            arrayJbuttonsTick[index].setEnabled(false);
        }
        
    }
    
    
    /**
     * When a Tick button in GUI gets clicked this means that the food is ready.
     * This function changes the ORDER_STATUS to "FOOD READY".
     * @param YesNo is the Decision made by the user on YES_NO confirmation.
     * @param index is the index of the arrays and ArrayLists used.
     */
    public void ChangeOrderStatusOnTick(int YesNo,int index)
    {
        Connection conn = JavaConnectDb.ConnectDb(); 
        Statement st;
        ResultSet rs;
        
        
        
        if(YesNo == 0) //0 means YES 
        {
            arrayJbuttonsTake[index].setBackground(Color.green); 
            arrayJbuttonsTake[index].setText("Take");
            
            
            arrayDeafaultListModel[index].clear(); //Need to clear the Model of a list in order to clear it
            arrayJlists[index].setModel(arrayDeafaultListModel[index]);
            
            
            arrayJlabels[index].setText("ORDER ID: ");
            
            
            //Prepei na kanw kai clear ta elements twn arrayLists orderIdList kai itemNameList gia na mpei kapoia allh paraggelia efoson auth eiani etoimh.
            
            try
            {
               
                PreparedStatement ps = conn.prepareStatement("UPDATE MAIN_ORDER SET ORDER_STATUS = ? WHERE ORDER_ID = ? ");
                ps.setString(1, "FOOD READY");
                ps.setString(2, arrayOrdersId[index]);
                
                ps.executeUpdate();
                ps.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            } 
        }
    }
    
    
    /**
     * Initialises all the variables needed.
     */
    public void InitialiseInstances()
    {
        //Initialise JLists array with the existing jLists in GUI.
        arrayJlists[0] = jList1;
        arrayJlists[1] = jList2;
        arrayJlists[2] = jList3;
        arrayJlists[3] = jList4;
        arrayJlists[4] = jList5;
        arrayJlists[5] = jList6;
        arrayJlists[6] = jList7;
        arrayJlists[7] = jList8;
        arrayJlists[8] = jList9;
        arrayJlists[9] = jList10;

        //Create many different DeafaultListModel lists.
        arrayDeafaultListModel[0] = new DefaultListModel();
        arrayDeafaultListModel[1] = new DefaultListModel();
        arrayDeafaultListModel[2] = new DefaultListModel();
        arrayDeafaultListModel[3] = new DefaultListModel();
        arrayDeafaultListModel[4] = new DefaultListModel();
        arrayDeafaultListModel[5] = new DefaultListModel();
        arrayDeafaultListModel[6] = new DefaultListModel();
        arrayDeafaultListModel[7] = new DefaultListModel();
        arrayDeafaultListModel[8] = new DefaultListModel();
        arrayDeafaultListModel[9] = new DefaultListModel();
        
        
        
        
        //Initialise JLabels array with the existing jLists in GUI.
        arrayJlabels[0] = lblOrderId1;
        arrayJlabels[1] = lblOrderId2;
        arrayJlabels[2] = lblOrderId3;
        arrayJlabels[3] = lblOrderId4;
        arrayJlabels[4] = lblOrderId5;
        arrayJlabels[5] = lblOrderId6;
        arrayJlabels[6] = lblOrderId7;
        arrayJlabels[7] = lblOrderId8;
        arrayJlabels[8] = lblOrderId9;
        arrayJlabels[9] = lblOrderId10;
        
        
        
        //Initialise JButtonsTake array with the existing jTakeButtons in GUI.
        arrayJbuttonsTake[0] = btnTake1;
        arrayJbuttonsTake[1] = btnTake2;
        arrayJbuttonsTake[2] = btnTake3;
        arrayJbuttonsTake[3] = btnTake4;
        arrayJbuttonsTake[4] = btnTake5;
        arrayJbuttonsTake[5] = btnTake6;
        arrayJbuttonsTake[6] = btnTake7;
        arrayJbuttonsTake[7] = btnTake8;
        arrayJbuttonsTake[8] = btnTake9;
        arrayJbuttonsTake[9] = btnTake10;
        
        
        
        //Initialise JButtonsTick array with the existing jTickButtons in GUI.
        arrayJbuttonsTick[0] = btnTick1;
        arrayJbuttonsTick[1] = btnTick2;
        arrayJbuttonsTick[2] = btnTick3;
        arrayJbuttonsTick[3] = btnTick4;
        arrayJbuttonsTick[4] = btnTick5;
        arrayJbuttonsTick[5] = btnTick6;
        arrayJbuttonsTick[6] = btnTick7;
        arrayJbuttonsTick[7] = btnTick8;
        arrayJbuttonsTick[8] = btnTick9;
        arrayJbuttonsTick[9] = btnTick10;
        
        
        
        //Disable all the Tick buttons when starting
        for(int i = 0; i <10; i++)
        {
            arrayJbuttonsTick[i].setEnabled(false);
        }
        
        

        
        //Initialise the arrays arrayOrdersId and arrayItemsNames
        arrayOrdersId = InitialiseContentOfArrayOrdersId();
        arrayItemsNames = InitialiseContentOfArrayItemsNames();
        arraySizeId = InitialiseContentOfArraySizeId();
        

        
        //Allocate Order details (Which ITEM_IDs correspond to which ORDER_IDs) Each row(ORDER_ID) contains contains a number of collumns(ITEM_ID) 
        for(int i=0; i < numberOfOrders; i++) 
        {
              AllocateItemsToOrderId(i);
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton8 = new javax.swing.JButton();
        pnlOrders1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        btnTake1 = new javax.swing.JButton();
        btnTick1 = new javax.swing.JButton();
        lblOrderId1 = new javax.swing.JLabel();
        pnlOrders2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        btnTake2 = new javax.swing.JButton();
        btnTick2 = new javax.swing.JButton();
        lblOrderId2 = new javax.swing.JLabel();
        pnlOrders3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList<>();
        btnTake3 = new javax.swing.JButton();
        btnTick3 = new javax.swing.JButton();
        lblOrderId3 = new javax.swing.JLabel();
        pnlOrders4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList4 = new javax.swing.JList<>();
        btnTake4 = new javax.swing.JButton();
        btnTick4 = new javax.swing.JButton();
        lblOrderId4 = new javax.swing.JLabel();
        pnlOrders5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList5 = new javax.swing.JList<>();
        btnTake5 = new javax.swing.JButton();
        btnTick5 = new javax.swing.JButton();
        lblOrderId5 = new javax.swing.JLabel();
        pnlOrders6 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jList6 = new javax.swing.JList<>();
        btnTake6 = new javax.swing.JButton();
        btnTick6 = new javax.swing.JButton();
        lblOrderId6 = new javax.swing.JLabel();
        pnlOrders7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jList7 = new javax.swing.JList<>();
        btnTake7 = new javax.swing.JButton();
        btnTick7 = new javax.swing.JButton();
        lblOrderId7 = new javax.swing.JLabel();
        pnlOrders8 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jList8 = new javax.swing.JList<>();
        btnTake8 = new javax.swing.JButton();
        btnTick8 = new javax.swing.JButton();
        lblOrderId8 = new javax.swing.JLabel();
        pnlOrders9 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jList9 = new javax.swing.JList<>();
        btnTake9 = new javax.swing.JButton();
        btnTick9 = new javax.swing.JButton();
        lblOrderId9 = new javax.swing.JLabel();
        pnlOrders10 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jList10 = new javax.swing.JList<>();
        btnTake10 = new javax.swing.JButton();
        btnTick10 = new javax.swing.JButton();
        lblOrderId10 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnLogOut = new javax.swing.JButton();

        jButton8.setText("jButton8");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setPreferredSize(new java.awt.Dimension(1475, 661));

        pnlOrders1.setBackground(new java.awt.Color(51, 51, 51));

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        btnTake1.setBackground(new java.awt.Color(51, 255, 0));
        btnTake1.setText("TAKE ORDER");
        btnTake1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake1ActionPerformed(evt);
            }
        });

        btnTick1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick1ActionPerformed(evt);
            }
        });

        lblOrderId1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId1.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId1.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders1Layout = new javax.swing.GroupLayout(pnlOrders1);
        pnlOrders1.setLayout(pnlOrders1Layout);
        pnlOrders1Layout.setHorizontalGroup(
            pnlOrders1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders1Layout.createSequentialGroup()
                .addComponent(btnTake1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(pnlOrders1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );
        pnlOrders1Layout.setVerticalGroup(
            pnlOrders1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders2.setBackground(new java.awt.Color(51, 51, 51));

        jList2.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList2);

        btnTake2.setBackground(new java.awt.Color(51, 255, 0));
        btnTake2.setText("TAKE ORDER");
        btnTake2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake2ActionPerformed(evt);
            }
        });

        btnTick2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick2ActionPerformed(evt);
            }
        });

        lblOrderId2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId2.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId2.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders2Layout = new javax.swing.GroupLayout(pnlOrders2);
        pnlOrders2.setLayout(pnlOrders2Layout);
        pnlOrders2Layout.setHorizontalGroup(
            pnlOrders2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders2Layout.createSequentialGroup()
                .addComponent(btnTake2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane2)
            .addGroup(pnlOrders2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );
        pnlOrders2Layout.setVerticalGroup(
            pnlOrders2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders3.setBackground(new java.awt.Color(51, 51, 51));

        jList3.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList3);

        btnTake3.setBackground(new java.awt.Color(51, 255, 0));
        btnTake3.setText("TAKE ORDER");
        btnTake3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake3ActionPerformed(evt);
            }
        });

        btnTick3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick3ActionPerformed(evt);
            }
        });

        lblOrderId3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId3.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId3.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders3Layout = new javax.swing.GroupLayout(pnlOrders3);
        pnlOrders3.setLayout(pnlOrders3Layout);
        pnlOrders3Layout.setHorizontalGroup(
            pnlOrders3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders3Layout.createSequentialGroup()
                .addComponent(btnTake3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick3, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane3)
            .addGroup(pnlOrders3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );
        pnlOrders3Layout.setVerticalGroup(
            pnlOrders3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders4.setBackground(new java.awt.Color(51, 51, 51));

        jList4.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList4);

        btnTake4.setBackground(new java.awt.Color(51, 255, 0));
        btnTake4.setText("TAKE ORDER");
        btnTake4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake4ActionPerformed(evt);
            }
        });

        btnTick4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick4ActionPerformed(evt);
            }
        });

        lblOrderId4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId4.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId4.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders4Layout = new javax.swing.GroupLayout(pnlOrders4);
        pnlOrders4.setLayout(pnlOrders4Layout);
        pnlOrders4Layout.setHorizontalGroup(
            pnlOrders4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders4Layout.createSequentialGroup()
                .addComponent(btnTake4, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick4, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane4)
            .addGroup(pnlOrders4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders4Layout.setVerticalGroup(
            pnlOrders4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTake4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders5.setBackground(new java.awt.Color(51, 51, 51));

        jList5.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane5.setViewportView(jList5);

        btnTake5.setBackground(new java.awt.Color(51, 255, 0));
        btnTake5.setText("TAKE ORDER");
        btnTake5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake5ActionPerformed(evt);
            }
        });

        btnTick5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick5ActionPerformed(evt);
            }
        });

        lblOrderId5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId5.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId5.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders5Layout = new javax.swing.GroupLayout(pnlOrders5);
        pnlOrders5.setLayout(pnlOrders5Layout);
        pnlOrders5Layout.setHorizontalGroup(
            pnlOrders5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders5Layout.createSequentialGroup()
                .addComponent(btnTake5, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane5)
            .addGroup(pnlOrders5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders5Layout.setVerticalGroup(
            pnlOrders5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders6.setBackground(new java.awt.Color(51, 51, 51));

        jList6.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane6.setViewportView(jList6);

        btnTake6.setBackground(new java.awt.Color(51, 255, 0));
        btnTake6.setText("TAKE ORDER");
        btnTake6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake6ActionPerformed(evt);
            }
        });

        btnTick6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick6ActionPerformed(evt);
            }
        });

        lblOrderId6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId6.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId6.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders6Layout = new javax.swing.GroupLayout(pnlOrders6);
        pnlOrders6.setLayout(pnlOrders6Layout);
        pnlOrders6Layout.setHorizontalGroup(
            pnlOrders6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders6Layout.createSequentialGroup()
                .addComponent(btnTake6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick6, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane6)
            .addGroup(pnlOrders6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );
        pnlOrders6Layout.setVerticalGroup(
            pnlOrders6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders7.setBackground(new java.awt.Color(51, 51, 51));

        jList7.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane7.setViewportView(jList7);

        btnTake7.setBackground(new java.awt.Color(51, 255, 0));
        btnTake7.setText("TAKE ORDER");
        btnTake7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake7ActionPerformed(evt);
            }
        });

        btnTick7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick7ActionPerformed(evt);
            }
        });

        lblOrderId7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId7.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId7.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders7Layout = new javax.swing.GroupLayout(pnlOrders7);
        pnlOrders7.setLayout(pnlOrders7Layout);
        pnlOrders7Layout.setHorizontalGroup(
            pnlOrders7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders7Layout.createSequentialGroup()
                .addComponent(btnTake7, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane7)
            .addGroup(pnlOrders7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders7Layout.setVerticalGroup(
            pnlOrders7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake7, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick7, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders8.setBackground(new java.awt.Color(51, 51, 51));

        jList8.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane8.setViewportView(jList8);

        btnTake8.setBackground(new java.awt.Color(51, 255, 0));
        btnTake8.setText("TAKE ORDER");
        btnTake8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake8ActionPerformed(evt);
            }
        });

        btnTick8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick8ActionPerformed(evt);
            }
        });

        lblOrderId8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId8.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId8.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders8Layout = new javax.swing.GroupLayout(pnlOrders8);
        pnlOrders8.setLayout(pnlOrders8Layout);
        pnlOrders8Layout.setHorizontalGroup(
            pnlOrders8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders8Layout.createSequentialGroup()
                .addComponent(btnTake8, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick8, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane8)
            .addGroup(pnlOrders8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId8, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders8Layout.setVerticalGroup(
            pnlOrders8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders9.setBackground(new java.awt.Color(51, 51, 51));

        jList9.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane9.setViewportView(jList9);

        btnTake9.setBackground(new java.awt.Color(51, 255, 0));
        btnTake9.setText("TAKE ORDER");
        btnTake9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake9ActionPerformed(evt);
            }
        });

        btnTick9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick9ActionPerformed(evt);
            }
        });

        lblOrderId9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId9.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId9.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders9Layout = new javax.swing.GroupLayout(pnlOrders9);
        pnlOrders9.setLayout(pnlOrders9Layout);
        pnlOrders9Layout.setHorizontalGroup(
            pnlOrders9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders9Layout.createSequentialGroup()
                .addComponent(btnTake9, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane9)
            .addGroup(pnlOrders9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId9, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders9Layout.setVerticalGroup(
            pnlOrders9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake9, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick9, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pnlOrders10.setBackground(new java.awt.Color(51, 51, 51));

        jList10.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane10.setViewportView(jList10);

        btnTake10.setBackground(new java.awt.Color(51, 255, 0));
        btnTake10.setText("TAKE ORDER");
        btnTake10.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnTake10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTake10ActionPerformed(evt);
            }
        });

        btnTick10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick10ActionPerformed(evt);
            }
        });

        lblOrderId10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId10.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId10.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders10Layout = new javax.swing.GroupLayout(pnlOrders10);
        pnlOrders10.setLayout(pnlOrders10Layout);
        pnlOrders10Layout.setHorizontalGroup(
            pnlOrders10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders10Layout.createSequentialGroup()
                .addComponent(btnTake10, javax.swing.GroupLayout.DEFAULT_SIZE, 196, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick10, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane10)
            .addGroup(pnlOrders10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId10, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlOrders10Layout.setVerticalGroup(
            pnlOrders10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlOrders10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTake10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTick10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        btnLogOut.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        btnLogOut.setForeground(new java.awt.Color(0, 0, 153));
        btnLogOut.setText("Log out");
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLogOut, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLogOut)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlOrders6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlOrders1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(99, 99, 99)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlOrders7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlOrders2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(77, 77, 77))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlOrders1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlOrders8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(52, 52, 52)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnTake7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake7ActionPerformed
        btnTake7.setText("Order Taken -\nTick when finish");
        btnTake7.setBackground(Color.red);
        btnTick7.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,6);
    }//GEN-LAST:event_btnTake7ActionPerformed

    private void btnTake8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake8ActionPerformed
        btnTake8.setText("Order Taken -\nTick when finish");
        btnTake8.setBackground(Color.red);
        btnTick8.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,7);
    }//GEN-LAST:event_btnTake8ActionPerformed

    private void btnTake3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake3ActionPerformed
        btnTake3.setText("Order Taken -\nTick when finish");
        btnTake3.setBackground(Color.red);
        btnTick3.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,2);
    }//GEN-LAST:event_btnTake3ActionPerformed

    private void btnTake1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake1ActionPerformed
        btnTake1.setText("Order Taken -\nTick when finish");
        btnTake1.setBackground(Color.red);
        btnTick1.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,0);
    }//GEN-LAST:event_btnTake1ActionPerformed

    private void btnTick1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick1ActionPerformed
              
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,0);        //Giati sto jPanel 1 vriskontai ta stoixeia ths paraggelias pou vrisketai sto index 0 twn arrayLists orderIdList kai itemNameList
    }//GEN-LAST:event_btnTick1ActionPerformed

    private void btnTake10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake10ActionPerformed
        btnTake10.setText("Order Taken -\nTick when finish");
        btnTake10.setBackground(Color.red);
        btnTick10.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,9);
    }//GEN-LAST:event_btnTake10ActionPerformed

    private void btnTake9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake9ActionPerformed
        btnTake9.setText("Order Taken -\nTick when finish");
        btnTake9.setBackground(Color.red);
        btnTick9.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,8);
    }//GEN-LAST:event_btnTake9ActionPerformed

    private void btnTake6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake6ActionPerformed
        btnTake6.setText("Order Taken -\nTick when finish");
        btnTake6.setBackground(Color.red);
        btnTick6.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,5);
    }//GEN-LAST:event_btnTake6ActionPerformed

    private void btnTake4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake4ActionPerformed
        btnTake4.setText("Order Taken -\nTick when finish");
        btnTake4.setBackground(Color.red);
        
        
        
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,3);
        
        
        //WHY IS THIS HERE???? HOW DID IT GO THERE??
        btnTick4.setEnabled(true);    }//GEN-LAST:event_btnTake4ActionPerformed

    private void btnTake2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake2ActionPerformed
        btnTake2.setText("Order Taken -\nTick when finish");
        btnTake2.setBackground(Color.red);
        btnTick2.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,1);
    }//GEN-LAST:event_btnTake2ActionPerformed

    private void btnTake5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTake5ActionPerformed
        btnTake5.setText("Order Taken -\nTick when finish");
        btnTake5.setBackground(Color.red);
        btnTick5.setEnabled(true);
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Take this order?","CHOOSE ORDER",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
         
        ChangeOrderStatusOnTake(yesOrNo,4);
    }//GEN-LAST:event_btnTake5ActionPerformed

    private void btnTick2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick2ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,1);
    }//GEN-LAST:event_btnTick2ActionPerformed

    private void btnTick3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick3ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,2);
    }//GEN-LAST:event_btnTick3ActionPerformed

    private void btnTick4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick4ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,3);
    }//GEN-LAST:event_btnTick4ActionPerformed

    private void btnTick5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick5ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,4);
    }//GEN-LAST:event_btnTick5ActionPerformed

    private void btnTick6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick6ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,5);
    }//GEN-LAST:event_btnTick6ActionPerformed

    private void btnTick7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick7ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,6);
    }//GEN-LAST:event_btnTick7ActionPerformed

    private void btnTick8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick8ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,7);
    }//GEN-LAST:event_btnTick8ActionPerformed

    private void btnTick9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick9ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,8);
    }//GEN-LAST:event_btnTick9ActionPerformed

    private void btnTick10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick10ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Have you completed the order?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                    
        ChangeOrderStatusOnTick(yesOrNo,9);
    }//GEN-LAST:event_btnTick10ActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        int yesNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to log out?","LOG OUT",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                
        if(yesNo == 0) 
        {
            this.hide();
            Login login = new Login();
            login.setVisible(true);
        }
    }//GEN-LAST:event_btnLogOutActionPerformed

    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ChefOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ChefOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ChefOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ChefOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                    new ChefOrdersPage().setVisible(true);
               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnTake1;
    private javax.swing.JButton btnTake10;
    private javax.swing.JButton btnTake2;
    private javax.swing.JButton btnTake3;
    private javax.swing.JButton btnTake4;
    private javax.swing.JButton btnTake5;
    private javax.swing.JButton btnTake6;
    private javax.swing.JButton btnTake7;
    private javax.swing.JButton btnTake8;
    private javax.swing.JButton btnTake9;
    private javax.swing.JButton btnTick1;
    private javax.swing.JButton btnTick10;
    private javax.swing.JButton btnTick2;
    private javax.swing.JButton btnTick3;
    private javax.swing.JButton btnTick4;
    private javax.swing.JButton btnTick5;
    private javax.swing.JButton btnTick6;
    private javax.swing.JButton btnTick7;
    private javax.swing.JButton btnTick8;
    private javax.swing.JButton btnTick9;
    private javax.swing.JButton jButton8;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList10;
    private javax.swing.JList<String> jList2;
    private javax.swing.JList<String> jList3;
    private javax.swing.JList<String> jList4;
    private javax.swing.JList<String> jList5;
    private javax.swing.JList<String> jList6;
    private javax.swing.JList<String> jList7;
    private javax.swing.JList<String> jList8;
    private javax.swing.JList<String> jList9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblOrderId1;
    private javax.swing.JLabel lblOrderId10;
    private javax.swing.JLabel lblOrderId2;
    private javax.swing.JLabel lblOrderId3;
    private javax.swing.JLabel lblOrderId4;
    private javax.swing.JLabel lblOrderId5;
    private javax.swing.JLabel lblOrderId6;
    private javax.swing.JLabel lblOrderId7;
    private javax.swing.JLabel lblOrderId8;
    private javax.swing.JLabel lblOrderId9;
    private javax.swing.JPanel pnlOrders1;
    private javax.swing.JPanel pnlOrders10;
    private javax.swing.JPanel pnlOrders2;
    private javax.swing.JPanel pnlOrders3;
    private javax.swing.JPanel pnlOrders4;
    private javax.swing.JPanel pnlOrders5;
    private javax.swing.JPanel pnlOrders6;
    private javax.swing.JPanel pnlOrders7;
    private javax.swing.JPanel pnlOrders8;
    private javax.swing.JPanel pnlOrders9;
    // End of variables declaration//GEN-END:variables
}
