/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DatabaseConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Panagiotis
 */
public class JavaConnectDb 
{
    public static Connection ConnectDb()
    {
       try
       {
           Class.forName("oracle.jdbc.OracleDriver");
           Connection con = DriverManager.getConnection("jdbc:oracle:thin:@larry.uopnet.plymouth.ac.uk:1521:orcl","PRCS251K","PRCS251K");
           return con;
       }
       catch(Exception e)
       {
           JOptionPane.showMessageDialog(null,e);
       }
       return null;
    }
}
