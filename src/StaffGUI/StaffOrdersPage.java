/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StaffGUI;

import DatabaseConnection.JavaConnectDb;
import LoginGUI.Login;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import restaurant.staffdatamodel.Item;
import restaurant.staffdatamodel.OrderDetails;

/**
 *
 * @author Panagiotis
 */
public class StaffOrdersPage extends javax.swing.JFrame {

   //The number of orders running at the moment. (like COUNT)
    private final int numberOfOrders =  InitialiseNumberOfOrders();
    //The total number of all ITEM_IDs in ORDER_DETAIL. (like COUNT)
    private final int numberOfItems = InitialiseNumberOfItems();

    //Contains the ORDER_IDs only once (DISTINCT).
    private String[] arrayOrdersId = new String[numberOfOrders];
    //Contains the ITEM_IDs (NOT DISTINCT).
    private String[] arrayItemsNames = new String[numberOfItems];

    //Contains All the jLists from GUI
    private JList[] arrayJlists = new JList[10]; 
    
    //Contains All the jLabels from GUI
    private JLabel[] arrayJlabels = new JLabel[10];
    
    //Contains All the jButtonsTake from GUI
    private JButton[] arrayJbuttonsTake = new JButton[10];
    
    //Contains All the jButtonsTick from GUI
    private JButton[] arrayJbuttonsTick = new JButton[10];

    
    
    //List Model
    private DefaultListModel[] arrayDeafaultListModel = new DefaultListModel[10];
    DefaultListModel list = new DefaultListModel();
    
    ArrayList<String> orderIdList = new ArrayList<String>();        
    ArrayList<String[]> itemNameList = new ArrayList<String[]>();
    
    
    
    
    /**
     * Initialises the GUI.
     */
    public StaffOrdersPage() 
    {
        initComponents();
        InitialiseInstances();
        this.setExtendedState(this.MAXIMIZED_BOTH);
        
        for(int i=0; i < numberOfOrders; i++) 
        {
            System.out.println(orderIdList.get(i));
            System.out.println(Arrays.toString(itemNameList.get(i)));
        }
        
        
        //Show orders in Jlists
        for(int i = 0; i < arrayOrdersId.length; i++)
        {
            ShowOrdersInJlists(i);
           
        }
    }

    
    /**
     * Counts how many different ORDER_ID exist in the ORDER_DETAIL.
     * @return the number of different ORDER_IDs.
     */
    public int InitialiseNumberOfOrders() 
    {
        int numberOfOrders = 0;
        
        ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
                
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT DISTINCT ORDER_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {            
                orderDetails = new OrderDetails(rs.getString("ORDER_ID"));
                orderIdList.add(orderDetails);   
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        numberOfOrders = orderIdList.size();
        return numberOfOrders;
    }
    
    
    /**
     * Counts how many ITEM_IDs exist in the ORDER_DETAIL.
     * @return the number of Items in ORDER_DETAIL table.
     */
    public int InitialiseNumberOfItems()
    {
        int numberOfItems = 0;
        
         ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT ITEM_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {
                orderDetails = new OrderDetails(rs.getString("ITEM_ID"));
                orderIdList.add(orderDetails);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        numberOfItems = orderIdList.size();
        return numberOfItems;
    }
    
    
    /**
     * Puts the ORDER_IDs in an array.
     * @return An array with all the ORDER_IDs in ORDER_DETAIL table.
     */
    public String[] InitialiseContentOfArrayOrdersId() 
    {
        String[] arrayContentOfArrayOrdersId = new String[numberOfOrders];
        
        ArrayList<OrderDetails> orderIdList = new ArrayList<OrderDetails>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        int i = 0;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT DISTINCT ORDER_ID FROM ORDER_DETAIL");
            OrderDetails orderDetails;
            while(rs.next())
            {
                orderDetails = new OrderDetails(rs.getString("ORDER_ID"));
                orderIdList.add(orderDetails);                
                arrayContentOfArrayOrdersId[i] = orderDetails.getOrderId();   
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
      return arrayContentOfArrayOrdersId;
    }
    
    
    /**
     * Puts the ITEM_IDs in an array
     * @return An array with all the ITEM_IDs in ORDER_DETAIL table.
     */
    public String[] InitialiseContentOfArrayItemsNames() 
    {
        String[] arrayContentOfArrayItemsId = new String[numberOfItems];
        
        ArrayList<Item> itemIdList = new ArrayList<Item>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        Statement st;
        ResultSet rs;
        
        int i = 0;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL");
            Item item;
            while(rs.next())
            {
                item = new Item(rs.getString("ITEM_NAME"));
                itemIdList.add(item);                
                arrayContentOfArrayItemsId[i] = item.getItemName();
                i++;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
      return arrayContentOfArrayItemsId;
    }
    
    
    
    /**
     * The 1st element of orderIdList corresponds on the 1st element of itemNameList the 2nd on the 2nd and so on..
     * By following this rule this method allocates the corresponding Items to an OrderId.
     * This function provides a whole order. Food and Drinks.
     * @param rowIndex is the index of orderIdList and itemNameList in every repetition.
     */ 
    public void AllocateItemsToOrderId(int rowIndex)
    {
        Connection conn = JavaConnectDb.ConnectDb();        
        
              
        String[] itemName = new String[numberOfItems]; //Auto prepei na allazei to length tou analoga me to posa items exei kathe paraggelia (Thelei sql COUNT). --> new String[numberOfItemsInAnOrder]; 
        
        
        
        
        
        Statement st;
        ResultSet rs;
        int i = 0;       
        try
        {
            st = conn.createStatement();
            
            //Store PIZZA
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'PIZZA'");          
                     
            orderIdList.add(arrayOrdersId[rowIndex]);
            
            
            while(rs.next())
            {
                               
                itemName[i] = rs.getString("ITEM_NAME");
                

                
                i++;

            }     
            
        
            //Store SIDES
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'DRINK'");          
            while(rs.next())
            {
                               
                itemName[i] = rs.getString("ITEM_NAME");
                

                
                i++;

            }   
            
            
            //Store DESERTS
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'DESERT'");          
            while(rs.next())
            {
                               
                itemName[i] = rs.getString("ITEM_NAME");
                

                
                i++;

            }   
            
            
            //Store SIDES
            rs = st.executeQuery("SELECT ITEM_NAME FROM ORDER_DETAIL WHERE ORDER_DETAIL.ORDER_ID =  " + arrayOrdersId[rowIndex] + " AND ORDER_DETAIL.ITEM_TYPE = 'SIDE'");          
            while(rs.next())
            {
                               
                itemName[i] = rs.getString("ITEM_NAME");
                

                
                i++;

            }   
            
            
            //itemNameList.add(new String[itemName.length]); //Set the size of each array in itemNameList ArrayList equal to the amount of the item in an order.
            itemNameList.add(itemName);
            
            
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    /**
     * THIS FUNCTION DOES NOT WORK PROPERLY
     * Hold all the JLists in an array.
     *  Check if any of these are empty(null).
     * If yes fill it with any available order.
     * @param index is the index of the arrays and ArrayLists used.
     */
    public void ShowOrdersInJlists(int index)
    {
        arrayJlabels[index].setText("ORDER ID: " + orderIdList.get(index));
        arrayJlists[index].setModel(arrayDeafaultListModel[index]);
        
        
        String[] arrayItemsInOrder = new String[numberOfItems];    
        arrayItemsInOrder = itemNameList.get(index);
                     
        int numberOfItemsInOrder = arrayItemsInOrder.length;
        
        
        //Display the items in rows
        for(int i = 0; i < numberOfItemsInOrder; i++) 
        {
            arrayDeafaultListModel[index].addElement(arrayItemsInOrder[i]);
        }
                
    }
    
    
    
    /**
     * When a Tick button in GUI gets clicked this means that the order is ready for collection.
     * This function changes the ORDER_STATUS to "READY FOR COLLECTION".
     * @param YesNo is the Decision made by the user on YES_NO confirmation.
     * @param index is the index of the arrays and ArrayLists used.
     */
    public void CheckOrderStatus(int YesNo,int index)
    {
        Connection conn = JavaConnectDb.ConnectDb(); 
        Statement st;
        ResultSet rs;
        
        
        
        if(YesNo == 0) //0 means YES 
        {
            
            
            arrayDeafaultListModel[index].clear(); //Need to clear the Model of a list in order to clear it
            arrayJlists[index].setModel(arrayDeafaultListModel[index]);
            
            
            arrayJlabels[index].setText("ORDER ID: ");
            
            
            //Prepei na kanw kai clear ta elements twn arrayLists orderIdList kai itemNameList gia na mpei kapoia allh paraggelia efoson auth eiani etoimh.
            
            try
            {
               
                PreparedStatement ps = conn.prepareStatement("UPDATE MAIN_ORDER SET ORDER_STATUS = ? WHERE ORDER_ID = ? ");
                ps.setString(1, "READY FOR COLLECTION");
                ps.setString(2, arrayOrdersId[index]);
                
                ps.executeUpdate();
                ps.close();
            }
            catch(Exception e)
            {
                e.printStackTrace();
            } 
        }
    }
    
    
    
    /**
     * Initialises all the variables needed.
     */
    public void InitialiseInstances()
    {
        //Initialise JLists array with the existing jLists in GUI.
        arrayJlists[0] = jList1;
        arrayJlists[1] = jList2;
        arrayJlists[2] = jList3;
        arrayJlists[3] = jList4;
        arrayJlists[4] = jList5;
        arrayJlists[5] = jList6;
        arrayJlists[6] = jList7;
        arrayJlists[7] = jList8;
        arrayJlists[8] = jList9;
        arrayJlists[9] = jList10;

        //Create many different DeafaultListModel lists.
        arrayDeafaultListModel[0] = new DefaultListModel();
        arrayDeafaultListModel[1] = new DefaultListModel();
        arrayDeafaultListModel[2] = new DefaultListModel();
        arrayDeafaultListModel[3] = new DefaultListModel();
        arrayDeafaultListModel[4] = new DefaultListModel();
        arrayDeafaultListModel[5] = new DefaultListModel();
        arrayDeafaultListModel[6] = new DefaultListModel();
        arrayDeafaultListModel[7] = new DefaultListModel();
        arrayDeafaultListModel[8] = new DefaultListModel();
        arrayDeafaultListModel[9] = new DefaultListModel();
        
        
        
        
        //Initialise JLabels array with the existing jLists in GUI.
        arrayJlabels[0] = lblOrderId1;
        arrayJlabels[1] = lblOrderId2;
        arrayJlabels[2] = lblOrderId3;
        arrayJlabels[3] = lblOrderId4;
        arrayJlabels[4] = lblOrderId5;
        arrayJlabels[5] = lblOrderId6;
        arrayJlabels[6] = lblOrderId7;
        arrayJlabels[7] = lblOrderId8;
        arrayJlabels[8] = lblOrderId9;
        arrayJlabels[9] = lblOrderId10;
        
        
        
        
        //Initialise JButtonsTick array with the existing jTickButtons in GUI.
        arrayJbuttonsTick[0] = btnTick1;
        arrayJbuttonsTick[1] = btnTick2;
        arrayJbuttonsTick[2] = btnTick3;
        arrayJbuttonsTick[3] = btnTick4;
        arrayJbuttonsTick[4] = btnTick5;
        arrayJbuttonsTick[5] = btnTick6;
        arrayJbuttonsTick[6] = btnTick7;
        arrayJbuttonsTick[7] = btnTick8;
        arrayJbuttonsTick[8] = btnTick9;
        arrayJbuttonsTick[9] = btnTick10;
         

        
        //Initialise the arrays arrayOrdersId and arrayItemsNames
        arrayOrdersId = InitialiseContentOfArrayOrdersId();
        arrayItemsNames = InitialiseContentOfArrayItemsNames();
        

        //Allocate Order details (Which ITEM_IDs correspond to which ORDER_IDs) Each row(ORDER_ID) contains contains a number of collumns(ITEM_ID) 
        for(int i=0; i < numberOfOrders; i++) 
        {
              AllocateItemsToOrderId(i);
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlOrders1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        btnTick1 = new javax.swing.JButton();
        lblOrderId1 = new javax.swing.JLabel();
        pnlOrders2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        btnTick2 = new javax.swing.JButton();
        lblOrderId2 = new javax.swing.JLabel();
        pnlOrders3 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList3 = new javax.swing.JList<>();
        btnTick3 = new javax.swing.JButton();
        lblOrderId3 = new javax.swing.JLabel();
        pnlOrders4 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList4 = new javax.swing.JList<>();
        btnTick4 = new javax.swing.JButton();
        lblOrderId4 = new javax.swing.JLabel();
        pnlOrders5 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jList5 = new javax.swing.JList<>();
        btnTick5 = new javax.swing.JButton();
        lblOrderId5 = new javax.swing.JLabel();
        pnlOrders6 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jList6 = new javax.swing.JList<>();
        btnTick6 = new javax.swing.JButton();
        lblOrderId6 = new javax.swing.JLabel();
        pnlOrders7 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jList7 = new javax.swing.JList<>();
        btnTick7 = new javax.swing.JButton();
        lblOrderId7 = new javax.swing.JLabel();
        pnlOrders8 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        jList8 = new javax.swing.JList<>();
        btnTick8 = new javax.swing.JButton();
        lblOrderId8 = new javax.swing.JLabel();
        pnlOrders9 = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jList9 = new javax.swing.JList<>();
        btnTick9 = new javax.swing.JButton();
        lblOrderId9 = new javax.swing.JLabel();
        pnlOrders10 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        jList10 = new javax.swing.JList<>();
        btnTick10 = new javax.swing.JButton();
        lblOrderId10 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlOrders1.setBackground(new java.awt.Color(51, 51, 51));

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        btnTick1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick1.setText("Ready to go");
        btnTick1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick1ActionPerformed(evt);
            }
        });

        lblOrderId1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId1.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId1.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders1Layout = new javax.swing.GroupLayout(pnlOrders1);
        pnlOrders1.setLayout(pnlOrders1Layout);
        pnlOrders1Layout.setHorizontalGroup(
            pnlOrders1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(pnlOrders1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders1Layout.setVerticalGroup(
            pnlOrders1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders2.setBackground(new java.awt.Color(51, 51, 51));

        jList2.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList2);

        btnTick2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick2.setText("Ready to go");
        btnTick2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick2ActionPerformed(evt);
            }
        });

        lblOrderId2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId2.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId2.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders2Layout = new javax.swing.GroupLayout(pnlOrders2);
        pnlOrders2.setLayout(pnlOrders2Layout);
        pnlOrders2Layout.setHorizontalGroup(
            pnlOrders2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
            .addGroup(pnlOrders2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders2Layout.setVerticalGroup(
            pnlOrders2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders3.setBackground(new java.awt.Color(51, 51, 51));

        jList3.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList3);

        btnTick3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick3.setText("Ready to go");
        btnTick3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick3ActionPerformed(evt);
            }
        });

        lblOrderId3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId3.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId3.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders3Layout = new javax.swing.GroupLayout(pnlOrders3);
        pnlOrders3.setLayout(pnlOrders3Layout);
        pnlOrders3Layout.setHorizontalGroup(
            pnlOrders3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
            .addGroup(pnlOrders3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders3Layout.setVerticalGroup(
            pnlOrders3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders4.setBackground(new java.awt.Color(51, 51, 51));

        jList4.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList4);

        btnTick4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick4.setText("Ready to go");
        btnTick4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick4ActionPerformed(evt);
            }
        });

        lblOrderId4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId4.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId4.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders4Layout = new javax.swing.GroupLayout(pnlOrders4);
        pnlOrders4.setLayout(pnlOrders4Layout);
        pnlOrders4Layout.setHorizontalGroup(
            pnlOrders4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
            .addGroup(pnlOrders4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId4, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders4Layout.setVerticalGroup(
            pnlOrders4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders5.setBackground(new java.awt.Color(51, 51, 51));

        jList5.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane5.setViewportView(jList5);

        btnTick5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick5.setText("Ready to go");
        btnTick5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick5ActionPerformed(evt);
            }
        });

        lblOrderId5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId5.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId5.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders5Layout = new javax.swing.GroupLayout(pnlOrders5);
        pnlOrders5.setLayout(pnlOrders5Layout);
        pnlOrders5Layout.setHorizontalGroup(
            pnlOrders5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5)
            .addGroup(pnlOrders5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId5, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders5Layout.setVerticalGroup(
            pnlOrders5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders6.setBackground(new java.awt.Color(51, 51, 51));

        jList6.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane6.setViewportView(jList6);

        btnTick6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick6.setText("Ready to go");
        btnTick6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick6ActionPerformed(evt);
            }
        });

        lblOrderId6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId6.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId6.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders6Layout = new javax.swing.GroupLayout(pnlOrders6);
        pnlOrders6.setLayout(pnlOrders6Layout);
        pnlOrders6Layout.setHorizontalGroup(
            pnlOrders6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6)
            .addGroup(pnlOrders6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId6, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders6Layout.setVerticalGroup(
            pnlOrders6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders7.setBackground(new java.awt.Color(51, 51, 51));

        jList7.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane7.setViewportView(jList7);

        btnTick7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick7.setText("Ready to go");
        btnTick7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick7ActionPerformed(evt);
            }
        });

        lblOrderId7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId7.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId7.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders7Layout = new javax.swing.GroupLayout(pnlOrders7);
        pnlOrders7.setLayout(pnlOrders7Layout);
        pnlOrders7Layout.setHorizontalGroup(
            pnlOrders7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7)
            .addGroup(pnlOrders7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId7, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders7Layout.setVerticalGroup(
            pnlOrders7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick7, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders8.setBackground(new java.awt.Color(51, 51, 51));

        jList8.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane8.setViewportView(jList8);

        btnTick8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick8.setText("Ready to go");
        btnTick8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick8ActionPerformed(evt);
            }
        });

        lblOrderId8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId8.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId8.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders8Layout = new javax.swing.GroupLayout(pnlOrders8);
        pnlOrders8.setLayout(pnlOrders8Layout);
        pnlOrders8Layout.setHorizontalGroup(
            pnlOrders8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane8)
            .addGroup(pnlOrders8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId8, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders8Layout.setVerticalGroup(
            pnlOrders8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick8, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders9.setBackground(new java.awt.Color(51, 51, 51));

        jList9.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane9.setViewportView(jList9);

        btnTick9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick9.setText("Ready to go");
        btnTick9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick9ActionPerformed(evt);
            }
        });

        lblOrderId9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId9.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId9.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders9Layout = new javax.swing.GroupLayout(pnlOrders9);
        pnlOrders9.setLayout(pnlOrders9Layout);
        pnlOrders9Layout.setHorizontalGroup(
            pnlOrders9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane9)
            .addGroup(pnlOrders9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId9, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders9Layout.setVerticalGroup(
            pnlOrders9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick9, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnlOrders10.setBackground(new java.awt.Color(51, 51, 51));

        jList10.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "NO ORDER AVAILABLE" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane10.setViewportView(jList10);

        btnTick10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ChefGUI/Ok-20.png"))); // NOI18N
        btnTick10.setText("Ready to go");
        btnTick10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTick10ActionPerformed(evt);
            }
        });

        lblOrderId10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblOrderId10.setForeground(new java.awt.Color(255, 0, 0));
        lblOrderId10.setText("ORDER ID");

        javax.swing.GroupLayout pnlOrders10Layout = new javax.swing.GroupLayout(pnlOrders10);
        pnlOrders10.setLayout(pnlOrders10Layout);
        pnlOrders10Layout.setHorizontalGroup(
            pnlOrders10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10)
            .addGroup(pnlOrders10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId10, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
            .addComponent(btnTick10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlOrders10Layout.setVerticalGroup(
            pnlOrders10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrders10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrderId10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTick10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setForeground(new java.awt.Color(51, 51, 51));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 153));
        jButton1.setText("Log out");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlOrders1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlOrders6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(98, 98, 98)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlOrders7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(96, 96, 96)
                                .addComponent(pnlOrders10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlOrders2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(99, 99, 99)
                                .addComponent(pnlOrders4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(96, 96, 96)
                                .addComponent(pnlOrders5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlOrders1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOrders5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlOrders6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlOrders7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(73, 73, 73)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnlOrders8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlOrders9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnlOrders10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(128, 128, 128)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTick9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick9ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,8);
    }//GEN-LAST:event_btnTick9ActionPerformed

    private void btnTick2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick2ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,1);
    }//GEN-LAST:event_btnTick2ActionPerformed

    private void btnTick6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick6ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,5);
    }//GEN-LAST:event_btnTick6ActionPerformed

    private void btnTick8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick8ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,7);
    }//GEN-LAST:event_btnTick8ActionPerformed

    private void btnTick10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick10ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,9);
    }//GEN-LAST:event_btnTick10ActionPerformed

    private void btnTick4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick4ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,3);
    }//GEN-LAST:event_btnTick4ActionPerformed

    private void btnTick7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick7ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,6);
    }//GEN-LAST:event_btnTick7ActionPerformed

    private void btnTick1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick1ActionPerformed

        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,0);        //Giati sto jPanel 1 vriskontai ta stoixeia ths paraggelias pou vrisketai sto index 0 twn arrayLists orderIdList kai itemNameList
    }//GEN-LAST:event_btnTick1ActionPerformed

    private void btnTick3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick3ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,2);
    }//GEN-LAST:event_btnTick3ActionPerformed

    private void btnTick5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTick5ActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Is everything ready to go?","Order Completion Confirmation",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);

        CheckOrderStatus(yesOrNo,4);
    }//GEN-LAST:event_btnTick5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int yesNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to log out?","LOG OUT",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                
        if(yesNo == 0) 
        {
            this.hide();
            Login login = new Login();
            login.setVisible(true);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StaffOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StaffOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StaffOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StaffOrdersPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StaffOrdersPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnTick1;
    private javax.swing.JButton btnTick10;
    private javax.swing.JButton btnTick2;
    private javax.swing.JButton btnTick3;
    private javax.swing.JButton btnTick4;
    private javax.swing.JButton btnTick5;
    private javax.swing.JButton btnTick6;
    private javax.swing.JButton btnTick7;
    private javax.swing.JButton btnTick8;
    private javax.swing.JButton btnTick9;
    private javax.swing.JButton jButton1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList10;
    private javax.swing.JList<String> jList2;
    private javax.swing.JList<String> jList3;
    private javax.swing.JList<String> jList4;
    private javax.swing.JList<String> jList5;
    private javax.swing.JList<String> jList6;
    private javax.swing.JList<String> jList7;
    private javax.swing.JList<String> jList8;
    private javax.swing.JList<String> jList9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JLabel lblOrderId1;
    private javax.swing.JLabel lblOrderId10;
    private javax.swing.JLabel lblOrderId2;
    private javax.swing.JLabel lblOrderId3;
    private javax.swing.JLabel lblOrderId4;
    private javax.swing.JLabel lblOrderId5;
    private javax.swing.JLabel lblOrderId6;
    private javax.swing.JLabel lblOrderId7;
    private javax.swing.JLabel lblOrderId8;
    private javax.swing.JLabel lblOrderId9;
    private javax.swing.JPanel pnlOrders1;
    private javax.swing.JPanel pnlOrders10;
    private javax.swing.JPanel pnlOrders2;
    private javax.swing.JPanel pnlOrders3;
    private javax.swing.JPanel pnlOrders4;
    private javax.swing.JPanel pnlOrders5;
    private javax.swing.JPanel pnlOrders6;
    private javax.swing.JPanel pnlOrders7;
    private javax.swing.JPanel pnlOrders8;
    private javax.swing.JPanel pnlOrders9;
    // End of variables declaration//GEN-END:variables
}
