
package restaurant.staffdatamodel;

/**
 *
 * @author Panagiotis
 */
public class Ingredient 
{
    private Integer ingredientId;
    private String ingredientName;
    private Integer ingredientQuantity;
    
    
    
    public Ingredient(Integer IngredientId, String IngridientName, Integer IngredientQuantity)
    {
        this.ingredientId = IngredientId;
        this.ingredientName = IngridientName;
        this.ingredientQuantity = IngredientQuantity;
    }
    
    
    

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public Integer getIngredientQuantity() {
        return ingredientQuantity;
    }

    public void setIngredientQuantity(Integer ingredientQuantity) {
        this.ingredientQuantity = ingredientQuantity;
    }
    
    
}
