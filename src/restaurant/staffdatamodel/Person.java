/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.staffdatamodel;

import javax.swing.JOptionPane;

/**
 *
 * @author Panagiotis
 */
public class Person 
{
     private Integer id; //Integer so it can be set null (int cant be set null)
    private String firstName;
    private String lastName;
    private String staffType;
    private String phoneNumber;
    private String bankAccount;
    private String salaryRate;
    private String emailUsername;
    private String password;
    private String salt;
    
    
    
    public Person(Integer Id, String FirstName, String LastName, String StaffType,
            String PhoneNumber, String BankAccount, String SalaryRate, String EmailUsername, String Password)
    {
        this.id = Id;
        this.firstName = FirstName;
        this.lastName = LastName;
        this.staffType = StaffType;
        this.phoneNumber = PhoneNumber;
        this.bankAccount = BankAccount;
        this.salaryRate = SalaryRate;
        this.emailUsername = EmailUsername;
        this.password = Password;
    }
    
    //Used for login
    public Person(String EmailUsername, String Password, String StaffType, String Salt)
    {
        this.emailUsername = EmailUsername;
        this.password = Password;
        this.staffType = StaffType;
        this.salt = Salt;
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStaffType() {
        return staffType;
    }
    public String getSalt() {
        return salt;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getSalaryRate() {
        return salaryRate;
    }

    public void setSalaryRate(String salaryRate) {
        this.salaryRate = salaryRate;
    }

    public String getEmailUsername() {
        return emailUsername;
    }

    public void setEmailUsername(String emailUsername) {
        this.emailUsername = emailUsername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }




    
    
    
    
}
