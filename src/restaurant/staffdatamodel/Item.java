
package restaurant.staffdatamodel;

/**
 *
 * @author Panagiotis
 */
public class Item 
{
    private Integer itemId; 
    private String itemName;
    private byte[] itemPicture;
    private String itemType;
    
    private Integer quantity;
    private String size;
    
    
    public Item()
    {
        
    }
    
    public Item(int ItemId)
    {
        this.itemId = ItemId;
    }


    public Item(String ItemName)
    {
        this.itemName = ItemName;
    }
    
    
    public Item(int ItemId, String ItemName, String ItemType)
    {
        this.itemId = ItemId;
        this.itemName = ItemName;
        
        this.itemType = ItemType;
    }

    public Item(int ItemId, String ItemName, byte[] ItemPicture, String ItemType)
    {
        this.itemId = ItemId;
        this.itemName = ItemName;
       
        this.itemPicture = ItemPicture;
        this.itemType = ItemType;
    }

    public Item(String Name, Integer Quantity, String Size)
    {
        this.itemName = Name;
        this.quantity = Quantity;
        this.size = Size;
    }
    
    
    
    public Integer getItemId()
    {
        return itemId;
    }

    public void setItemId(Integer itemId) 
    {
        this.itemId = itemId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    

    public byte[] getItemPicture() 
    {
        return itemPicture;
    }

    public void setItemPicture(byte[] itemPicture) 
    {
        this.itemPicture = itemPicture;
    }

    public String getItemType() 
    {
        return itemType;
    }

    public void setItemType(String itemType) 
    {
        this.itemType = itemType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    
    
    
}
