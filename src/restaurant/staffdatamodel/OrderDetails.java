
package restaurant.staffdatamodel;


public class OrderDetails 
{
    private int orderDetailId;
    private String orderId;
    private String itemId;
    private String itemName;
    private int quantity;
    private String size;
    
    //Used to count how many orders running at a moment
    public OrderDetails(String OrderId)
    {
        this.orderId = OrderId;
    }
    
    
    
    
    public OrderDetails(String OrderId, String ItemId, int Quantity)
    {
        this.orderId = OrderId;
        this.itemId = ItemId;
        this.quantity = Quantity;
    }
    
    
    public OrderDetails(int Quantity, String Size)
    {
        this.quantity = Quantity;
        this.size = Size;                    
    }

    public int getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(int orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

   
    
    
    
    
    
}
