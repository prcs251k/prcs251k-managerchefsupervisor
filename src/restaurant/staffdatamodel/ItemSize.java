/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.staffdatamodel;

/**
 *
 * @author louka
 */
public class ItemSize {
    private Integer ItemSizeId;
    private Integer ItemSizeUnitPrice;
    private String ItemName;
    private Integer ItemId;
    private String ItemSizeType;
    
    
    
    
    public ItemSize(int sideId,int sizePrice, String itemName,int itemId,String sizeType )
    {
       this.ItemSizeId=  sideId;
       this.ItemSizeUnitPrice=sizePrice;
       this.ItemName=itemName;
       this.ItemId=itemId;
       this.ItemSizeType=sizeType;
       
    }

    public Integer getItemSizeId() {
        return ItemSizeId;
    }

    public void setItemSizeId(Integer ItemSizeId) {
        this.ItemSizeId = ItemSizeId;
    }

    public Integer getItemSizeUnitPrice() {
        return ItemSizeUnitPrice;
    }

    public void setItemSizeUnitPrice(Integer ItemSizeUnitPrice) {
        this.ItemSizeUnitPrice = ItemSizeUnitPrice;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String ItemName) {
        this.ItemName = ItemName;
    }

    public Integer getItemId() {
        return ItemId;
    }

    public void setItemId(Integer ItemId) {
        this.ItemId = ItemId;
    }

    public String getItemSizeType() {
        return ItemSizeType;
    }

    public void setItemSizeType(String ItemSizeType) {
        this.ItemSizeType = ItemSizeType;
    }
    
    
    
    
}
