/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant.staffdatamodel;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Panagiotis
 */
@Entity
@Table(name = "ORDER_HISTORY", catalog = "", schema = "PRCS251K")
@NamedQueries({
    @NamedQuery(name = "OrderHistory.findAll", query = "SELECT o FROM OrderHistory o"),
    @NamedQuery(name = "OrderHistory.findByOrderHistoryId", query = "SELECT o FROM OrderHistory o WHERE o.orderHistoryId = :orderHistoryId"),
    @NamedQuery(name = "OrderHistory.findByOrderId", query = "SELECT o FROM OrderHistory o WHERE o.orderId = :orderId"),
    @NamedQuery(name = "OrderHistory.findByCustomerId", query = "SELECT o FROM OrderHistory o WHERE o.customerId = :customerId"),
    @NamedQuery(name = "OrderHistory.findByTotalcost", query = "SELECT o FROM OrderHistory o WHERE o.totalcost = :totalcost"),
    @NamedQuery(name = "OrderHistory.findByOrderDate", query = "SELECT o FROM OrderHistory o WHERE o.orderDate = :orderDate"),
    @NamedQuery(name = "OrderHistory.findByRiderId", query = "SELECT o FROM OrderHistory o WHERE o.riderId = :riderId")})
public class OrderHistory implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ORDER_HISTORY_ID")
    private String orderHistoryId;
    @Column(name = "ORDER_ID")
    private String orderId;
    @Column(name = "CUSTOMER_ID")
    private String customerId;
    @Column(name = "TOTALCOST")
    private String totalcost;
    @Column(name = "ORDER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;
    @Column(name = "RIDER_ID")
    private String riderId;

    public OrderHistory() {
    }

    public OrderHistory(String orderHistoryId) {
        this.orderHistoryId = orderHistoryId;
    }

    public String getOrderHistoryId() {
        return orderHistoryId;
    }

    public void setOrderHistoryId(String orderHistoryId) {
        String oldOrderHistoryId = this.orderHistoryId;
        this.orderHistoryId = orderHistoryId;
        changeSupport.firePropertyChange("orderHistoryId", oldOrderHistoryId, orderHistoryId);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        String oldOrderId = this.orderId;
        this.orderId = orderId;
        changeSupport.firePropertyChange("orderId", oldOrderId, orderId);
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        String oldCustomerId = this.customerId;
        this.customerId = customerId;
        changeSupport.firePropertyChange("customerId", oldCustomerId, customerId);
    }

    public String getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(String totalcost) {
        String oldTotalcost = this.totalcost;
        this.totalcost = totalcost;
        changeSupport.firePropertyChange("totalcost", oldTotalcost, totalcost);
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        Date oldOrderDate = this.orderDate;
        this.orderDate = orderDate;
        changeSupport.firePropertyChange("orderDate", oldOrderDate, orderDate);
    }

    public String getRiderId() {
        return riderId;
    }

    public void setRiderId(String riderId) {
        String oldRiderId = this.riderId;
        this.riderId = riderId;
        changeSupport.firePropertyChange("riderId", oldRiderId, riderId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderHistoryId != null ? orderHistoryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderHistory)) {
            return false;
        }
        OrderHistory other = (OrderHistory) object;
        if ((this.orderHistoryId == null && other.orderHistoryId != null) || (this.orderHistoryId != null && !this.orderHistoryId.equals(other.orderHistoryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ManagerGUI.OrderHistory[ orderHistoryId=" + orderHistoryId + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
