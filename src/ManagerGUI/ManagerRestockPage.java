/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagerGUI;

import DatabaseConnection.JavaConnectDb;
import LoginGUI.Login;
import static java.lang.Integer.parseInt;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import restaurant.staffdatamodel.Ingredient;
import restaurant.staffdatamodel.Item;

/**
 *
 * @author Panagiotis
 */
public class ManagerRestockPage extends javax.swing.JFrame 
{

    /**
     * Creates new form ManagerRestockPage
     */
    public ManagerRestockPage()
    {
        initComponents();
        this.setExtendedState(this.MAXIMIZED_BOTH);
        ShowIngredientsInTable();
    }

    
    /**
     * Gets data from table INGREDIENT 
     * @return an ArrayList containing objects of class Ingredient.
     */
    public ArrayList<Ingredient> getIngredientList()
    {
        ArrayList<Ingredient> ingredientList = new ArrayList<Ingredient>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        String query = "SELECT * FROM INGREDIENT";
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            Ingredient ingredient;
            while(rs.next())
            {
                ingredient = new Ingredient(rs.getInt("INGREDIENT_ID"), rs.getString("INGREDIENT_NAME"), rs.getInt("INGREDIENT_QTY"));
                ingredientList.add(ingredient);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return ingredientList;
    }
    
    
    /**
     * Display data in jTable
     */
    public void ShowIngredientsInTable()
    {
        ArrayList<Ingredient> list = getIngredientList();
        DefaultTableModel model = (DefaultTableModel)tblShowIngredientsDetails.getModel();
                 
        Object[] row = new Object[3];
        
        //Then display the data with the new changes
        for(int i = 0; i < list.size(); i++)
        {
            row[0] = list.get(i).getIngredientId();
            row[1] = list.get(i).getIngredientName();
            row[2] = list.get(i).getIngredientQuantity();
            
            model.addRow(row);
        }       
    }
    
    
    /**
     * Execute The SQL Query
     * @param query is the query to be executed.
     * @param message is the message to be displayed and inform user.
     */
    public void executeSqlQuery(String query, String message)
    {
        Connection conn = JavaConnectDb.ConnectDb();
        Statement st;
        
        try
        {
            st = conn.createStatement();
                    if((st.executeUpdate(query)) == 1)
                    {
                        //Refresh Table data
                        DefaultTableModel model = (DefaultTableModel) tblShowIngredientsDetails.getModel();
                        model.setRowCount(0);
                        
                        ShowIngredientsInTable();
                        
                        JOptionPane.showMessageDialog(null, "Data " + message + "Succesfully");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Data Not" + message);
                    }
        }
        catch(Exception ex)
        {
           ex.printStackTrace();
        }
    }

   
   
    
    /**
     * Search an Ingredient by name, Including case letters of low.
     */
    public void SearchByIngredientName()
    {
        String ingredientSearch = txtSearch.getText();
        
        ArrayList<Ingredient> ingredientsList = getIngredientList();
        DefaultTableModel model = (DefaultTableModel)tblShowIngredientsDetails.getModel();
        
        model.setRowCount(0);
        
        Object[] row = new Object[3];
        boolean exist = false;
        
        
        for(int i = 0; i < ingredientsList.size(); i++)
        {
            //If the searching ingredient exists..
            if(ingredientSearch.equalsIgnoreCase(ingredientsList.get(i).getIngredientName()))
            {
                exist = true;   

                row[0] = ingredientsList.get(i).getIngredientId();
                row[1] = ingredientsList.get(i).getIngredientName();
                row[2] = ingredientsList.get(i).getIngredientQuantity();

                model.addRow(row);
                break;
            } 
            //If it does not exist..
            else
            {
                exist = false;
            }
        }
        
 
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
    
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
  
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblShowIngredientsDetails = new javax.swing.JTable();
        btnSave = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnShowAll = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnLogOut = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Report", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 0, 24), new java.awt.Color(255, 0, 0))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(240, 240, 240));

        tblShowIngredientsDetails.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));
        tblShowIngredientsDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Ingredient ID", "Ingredient Name", "Ingredient Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblShowIngredientsDetails.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblShowIngredientsDetails);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Save_20.png"))); // NOI18N
        btnSave.setText("SAVE CHANGES");
        btnSave.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(240, 240, 240));
        jLabel1.setText("Search (by ingredient name): ");

        btnShowAll.setText("Show All");
        btnShowAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowAllActionPerformed(evt);
            }
        });

        txtSearch.setBackground(new java.awt.Color(51, 51, 51));
        txtSearch.setForeground(new java.awt.Color(240, 240, 240));
        txtSearch.setToolTipText("");
        txtSearch.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        btnSearch.setText("Search");
        btnSearch.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnLogOut.setForeground(new java.awt.Color(0, 0, 153));
        btnLogOut.setText("Log out");
        btnLogOut.setPreferredSize(new java.awt.Dimension(87, 29));
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnShowAll)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(283, 283, 283)
                                .addComponent(btnSave, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)))
                        .addGap(443, 443, 443))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(61, 61, 61)
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnLogOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnShowAll)
                        .addGap(24, 24, 24)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        
         ArrayList<Ingredient> currentDatabaseList = getIngredientList();
         DefaultTableModel model = (DefaultTableModel) tblShowIngredientsDetails.getModel();
         
         
         Connection conn = JavaConnectDb.ConnectDb();
         Statement st;
         
         
         int dialogButton = JOptionPane.showConfirmDialog (null, "Are you sure?","SAVE CHANGES", JOptionPane.YES_NO_OPTION);       
         if(dialogButton == JOptionPane.YES_OPTION)
         {         
            for(int i = 0; i < currentDatabaseList.size(); i++)
            {
               String query = "UPDATE INGREDIENT SET INGREDIENT_QTY= '"+tblShowIngredientsDetails.getValueAt(i,2)+"'WHERE INGREDIENT_ID = "+tblShowIngredientsDetails.getValueAt(i, 0);    

                try 
                {
                    st = conn.createStatement();
                    st.executeUpdate(query);
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(ManagerRestockPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            model.setRowCount(0);   //It will erase all the date displayed       
            ShowIngredientsInTable();   //And display it again with the new changes
         }
        else
         {
             model.setRowCount(0); 
             ShowIngredientsInTable();      
         }    
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnShowAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowAllActionPerformed
        DefaultTableModel model = (DefaultTableModel) tblShowIngredientsDetails.getModel();
        model.setRowCount(0); 
        txtSearch.setText("");
        ShowIngredientsInTable();
    }//GEN-LAST:event_btnShowAllActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.hide();
        
        ManagerMainPage mmp = new ManagerMainPage();
        mmp.setVisible(true);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SearchByIngredientName();
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        int yesNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to log out?","LOG OUT",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                
        if(yesNo == 0) 
        {
            this.hide();
            Login login = new Login();
            login.setVisible(true);
        }
    }//GEN-LAST:event_btnLogOutActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManagerRestockPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManagerRestockPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManagerRestockPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManagerRestockPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManagerRestockPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnShowAll;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblShowIngredientsDetails;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
