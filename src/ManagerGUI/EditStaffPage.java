/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagerGUI;

import DatabaseConnection.JavaConnectDb;
import LoginGUI.Login;
import ManagerGUI.ManagerMainPage;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleResultSet;
import restaurant.staffdatamodel.Person;

/**
 *
 * @author Panagiotis
 */
public class EditStaffPage extends javax.swing.JFrame 
{
    //Holds all the txtFields of the Gui.
    private JTextField[] arrayTextFields = new JTextField[8];
    
    
    public EditStaffPage()
    {
        initComponents();
        ShowPersonsInTable();
        
        //Set fullscreen
        this.setExtendedState(this.MAXIMIZED_BOTH); 
        
        //Initialise arrayTextFields
        arrayTextFields[0] = txtId;
        arrayTextFields[1] = txtFirstName;
        arrayTextFields[2] = txtLastName;
        arrayTextFields[3] = txtPhoneNumber;
        arrayTextFields[4] = txtBankAccount;
        arrayTextFields[5] = txtSalaryRate;
        arrayTextFields[6] = txtEmailUsername;
        arrayTextFields[7] = txtPassword;   
    }

    
    
    /**
     * Gets data from table STAFF in database.
     * @return an ArrayList containing objects of class Person.
     */
    public ArrayList<Person> getPersonList()
    {
        ArrayList<Person> personList = new ArrayList<Person>();
        Connection conn = JavaConnectDb.ConnectDb();
        
        String query = "SELECT * FROM STAFF"; 
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            Person person;
            while(rs.next())
            {   
                person = new Person(rs.getInt("STAFF_ID"),rs.getString("STAFF_NAME"), rs.getString("STAFF_SURNAME"), rs.getString("STAFF_TYPE"), 
                        rs.getString("STAFF_CONTACT_NO"), rs.getString("STAFF_BANK_ACCOUNT"), rs.getString("SALARY_RATE"),
                        rs.getString("EMAIL_USERNAME"), rs.getString("PASSWORD")); 
                
                personList.add(person);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
           
        return personList;    
    }
    
    
    
    
    /**
     * Display data in table
     */
    public void ShowPersonsInTable()
    {
        ArrayList<Person> list = getPersonList();
        DefaultTableModel model = (DefaultTableModel)tblShowPersons.getModel();
        Object[] row = new Object[9]; 
        
        for(int i = 0; i < list.size(); i++)
        {
            row[0] = list.get(i).getId();
            row[1] = list.get(i).getFirstName();
            row[2] = list.get(i).getLastName();
            row[3] = list.get(i).getStaffType();
            row[4] = list.get(i).getPhoneNumber();
            row[5] = list.get(i).getBankAccount();
            row[6] = list.get(i).getSalaryRate();
            row[7] = list.get(i).getEmailUsername();
            row[8] = list.get(i).getPassword();
            
            model.addRow(row);
        }    
    }
    
    
    
    /**
     * /Execute The SQL Query and print a message informing the user
     * @param query is the query to be executed.
     * @param message is the message to  be printed.
     */
    public void executeSqlQuery(String query, String message)
    {
        Connection conn = JavaConnectDb.ConnectDb();
        Statement st;
        
        try
        {
            st = conn.createStatement();
                    if((st.executeUpdate(query)) == 1)
                    {
                        //Refresh Table data
                        DefaultTableModel model = (DefaultTableModel) tblShowPersons.getModel();
                        model.setRowCount(0);
                        ShowPersonsInTable();
                        
                        JOptionPane.showMessageDialog(null,message + " Succesfully");
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, message);
                    }
        }
        catch(Exception ex)
        {
           ex.printStackTrace();
        }
    }
    
    
    /**
     * Clears all the textFields on GUI.
     */
    public void ClearTextFields()
    {
        txtId.setText("AUTOCOMPLETE");
        txtFirstName.setText("");
        txtLastName.setText("");
        txtPhoneNumber.setText("");
        txtBankAccount.setText("");
        txtSalaryRate.setText("");
        txtEmailUsername.setText("");
        txtPassword.setText("");
    }
    
    
    
    /**
     * Check if all fields are filled when editing a staff member
     * @return true if there is an empty text field and false if not.
     */
    public boolean MissingFields()
    {
        boolean isMissing = false;
        
        for(int i = 0; i < arrayTextFields.length; i++)
        {    
            if(arrayTextFields[i].getText().equals(""))
            {
                isMissing = true;
                JOptionPane.showMessageDialog(rootPane, "All fields must be filled.");
                break;
            }
        } 
        return isMissing;
    }
    
    
    /**
     * Searches for the staff members based on their staff type and displays them in jTable.
     */
    public void SearchStaffTypeInTable()
    {
        String SearchText=cmdSearchStaffType.getSelectedItem().toString(); //txtSearch.getText();
        ArrayList<Person> list = getPersonList();
        DefaultTableModel model = (DefaultTableModel)tblShowPersons.getModel();
        model.setRowCount(0);
        Object[] row = new Object[9]; 
       
        boolean exist = false;
        for(int i = 0; i < list.size(); i++)
        {  if(SearchText.equalsIgnoreCase(list.get(i).getStaffType()))
           {
                exist = true; 
                row[0] = list.get(i).getId();
                row[1] = list.get(i).getFirstName();
                row[2] = list.get(i).getLastName();  
                row[3] = list.get(i).getStaffType();
                row[4] = list.get(i).getPhoneNumber();
                row[5] = list.get(i).getBankAccount();
                row[6] = list.get(i).getSalaryRate();
                row[7] = list.get(i).getEmailUsername();
                row[8] = list.get(i).getPassword();

                model.addRow(row);

           } 
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
    
    
    /**
     * Searches for the staff members based on their name and displays them in jTable.
     */
    public void SearchByStaffName()
    {
        String SearchText=txtSearchName.getText();
        ArrayList<Person> list = getPersonList();
        DefaultTableModel model = (DefaultTableModel)tblShowPersons.getModel();
        model.setRowCount(0);
        Object[] row = new Object[9]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
       
        boolean exist = false;
        for(int i = 0; i < list.size(); i++)
        {  if(SearchText.equalsIgnoreCase(list.get(i).getFirstName()))
            {
                exist = true; 
                row[0] = list.get(i).getId();
                row[1] = list.get(i).getFirstName();
                row[2] = list.get(i).getLastName();  
                row[3] = list.get(i).getStaffType();
                row[4] = list.get(i).getPhoneNumber();
                row[5] = list.get(i).getBankAccount();
                row[6] = list.get(i).getSalaryRate();
                row[7] = list.get(i).getEmailUsername();
                row[8] = list.get(i).getPassword();
                
                model.addRow(row);

            } 
        
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
    
    
    
    
    /**
     * generates a random salt for user
     * @return salt that is entered into database
     */
    String makeSalt()
    {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        char[] salt = new char[5];
        Random rnd = new Random();
        int i = 0;
        while (salt.length < 5) 
        {
            int index = (rnd.nextInt() * SALTCHARS.length());
            salt[i] = SALTCHARS.charAt(index);
            i++;
        }
        String FinalSalt = salt.toString();
        return FinalSalt;

    }
    
    /**
     * hashes password, to be entered into database
     * @return hashed and salted password
     * @throws NoSuchAlgorithmException 
     */
    private String hashPassword(String salt) throws NoSuchAlgorithmException
    {
        
         byte[] input;
            input = (salt + txtPassword.getText()).getBytes();
           
           
           MessageDigest md = MessageDigest.getInstance("SHA-256");
           
           md.update(input);
           
           byte[] digest = md.digest();
           
           StringBuffer hexDigest = new StringBuffer();
           
           for(int i=0; i<digest.length;i++)
           {
               hexDigest.append(Integer.toString((digest[i]&0xff)+0x100,16).substring(1));
           }
           return hexDigest.toString();
    }
    
    
   
    
    
    
    
    
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnInsert = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtSearchName = new javax.swing.JTextField();
        txtFirstName = new javax.swing.JTextField();
        txtLastName = new javax.swing.JTextField();
        txtPhoneNumber = new javax.swing.JTextField();
        txtBankAccount = new javax.swing.JTextField();
        txtSalaryRate = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblShowPersons = new javax.swing.JTable();
        comboStaffType = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        btnLogOut = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtEmailUsername = new javax.swing.JTextField();
        txtPassword = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        cmdSearchStaffType = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Edit Staff", javax.swing.border.TitledBorder.LEADING, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 1, 24), new java.awt.Color(255, 0, 0))); // NOI18N

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(240, 240, 240));
        jLabel2.setText("First Name:");

        jLabel3.setForeground(new java.awt.Color(240, 240, 240));
        jLabel3.setText("Last Name:");

        jLabel4.setForeground(new java.awt.Color(240, 240, 240));
        jLabel4.setText("Phone Number:");

        jLabel5.setForeground(new java.awt.Color(240, 240, 240));
        jLabel5.setText("Bank Account: ");

        jLabel6.setForeground(new java.awt.Color(240, 240, 240));
        jLabel6.setText("Salary Rate:");

        btnInsert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Plus_20.png"))); // NOI18N
        btnInsert.setText("Insert");
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Update_20.png"))); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Delete_20.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(240, 240, 240));
        jLabel7.setText("Search Person (Name): ");

        txtSearchName.setBackground(new java.awt.Color(51, 51, 51));
        txtSearchName.setForeground(new java.awt.Color(240, 240, 240));
        txtSearchName.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtFirstName.setBackground(new java.awt.Color(51, 51, 51));
        txtFirstName.setForeground(new java.awt.Color(240, 240, 240));
        txtFirstName.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtLastName.setBackground(new java.awt.Color(51, 51, 51));
        txtLastName.setForeground(new java.awt.Color(240, 240, 240));
        txtLastName.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtPhoneNumber.setBackground(new java.awt.Color(51, 51, 51));
        txtPhoneNumber.setForeground(new java.awt.Color(240, 240, 240));
        txtPhoneNumber.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtBankAccount.setBackground(new java.awt.Color(51, 51, 51));
        txtBankAccount.setForeground(new java.awt.Color(240, 240, 240));
        txtBankAccount.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtSalaryRate.setBackground(new java.awt.Color(51, 51, 51));
        txtSalaryRate.setForeground(new java.awt.Color(240, 240, 240));
        txtSalaryRate.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        tblShowPersons.setBackground(new java.awt.Color(51, 51, 51));
        tblShowPersons.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));
        tblShowPersons.setForeground(new java.awt.Color(240, 240, 240));
        tblShowPersons.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "First Name", "Last Name", "Staff Type", "Phone Number", "Bank Account", "Salary Rate", "Email - Username", "Password"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblShowPersons.getTableHeader().setReorderingAllowed(false);
        tblShowPersons.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblShowPersonsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblShowPersons);
        if (tblShowPersons.getColumnModel().getColumnCount() > 0) {
            tblShowPersons.getColumnModel().getColumn(1).setResizable(false);
        }

        comboStaffType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MANAGER", "CHEF", "RIDER", "SUPERVISOR" }));

        jLabel8.setForeground(new java.awt.Color(240, 240, 240));
        jLabel8.setText("Staff Type:");

        btnLogOut.setForeground(new java.awt.Color(0, 0, 153));
        btnLogOut.setText("Log out");
        btnLogOut.setPreferredSize(new java.awt.Dimension(87, 29));
        btnLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogOutActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(240, 240, 240));
        jLabel9.setText("Email - Username");

        jLabel10.setForeground(new java.awt.Color(240, 240, 240));
        jLabel10.setText("Password");

        txtEmailUsername.setBackground(new java.awt.Color(51, 51, 51));
        txtEmailUsername.setForeground(new java.awt.Color(240, 240, 240));
        txtEmailUsername.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtPassword.setBackground(new java.awt.Color(51, 51, 51));
        txtPassword.setForeground(new java.awt.Color(240, 240, 240));
        txtPassword.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        jButton1.setText("Clear Text Fields");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(240, 240, 240));
        jLabel11.setText("ID: ");

        txtId.setEditable(false);
        txtId.setBackground(new java.awt.Color(51, 51, 51));
        txtId.setForeground(new java.awt.Color(240, 240, 240));
        txtId.setText("AUTOCOMPLETE ");
        txtId.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        cmdSearchStaffType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MANAGER", "CHEF", "SUPERVISOR", "RIDER" }));
        cmdSearchStaffType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdSearchStaffTypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack)
                    .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel8))
                        .addGap(80, 80, 80)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboStaffType, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(56, 56, 56)
                        .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(60, 60, 60)
                        .addComponent(txtBankAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(74, 74, 74)
                        .addComponent(txtSalaryRate, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(43, 43, 43)
                        .addComponent(txtEmailUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(90, 90, 90)
                        .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(jButton1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(78, 78, 78)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtId, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                            .addComponent(txtFirstName))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(83, 83, 83)
                        .addComponent(cmdSearchStaffType, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(379, 379, 379)
                        .addComponent(btnLogOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1))))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDelete, btnInsert, btnUpdate});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtBankAccount, txtEmailUsername, txtFirstName, txtId, txtLastName, txtPassword, txtPhoneNumber, txtSalaryRate});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnSearch, cmdSearchStaffType});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLogOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSearch)
                            .addComponent(txtSearchName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cmdSearchStaffType, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 21, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11)
                                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(comboStaffType, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtBankAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtSalaryRate, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtEmailUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9))
                                .addGap(18, 18, 18)
                                .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel10))
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                        .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnUpdate)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete)
                        .addGap(48, 48, 48)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDelete, btnInsert, btnUpdate});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnSearch, cmdSearchStaffType});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.hide();
        
        ManagerMainPage mmp = new ManagerMainPage();
        mmp.setVisible(true);
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void tblShowPersonsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblShowPersonsMouseClicked
        // Display Selected Row in TextFields
        
        int i = tblShowPersons.getSelectedRow();
        TableModel model = tblShowPersons.getModel();
        
        txtId.setText(model.getValueAt(i,0).toString());
        txtFirstName.setText(model.getValueAt(i,1).toString());
        txtLastName.setText(model.getValueAt(i,2).toString());   
        
        
        String comboCheck = model.getValueAt(i, 3).toString(); //Store the value of 3rd Collumn in a String variable.
        if(comboCheck.equals("MANAGER"))
        {
            comboStaffType.setSelectedIndex(0);
        }
        else if(comboCheck.equals("CHEF"))
        {
            comboStaffType.setSelectedIndex(1);
        }
        else if(comboCheck.equals("RIDER"))
        {
            comboStaffType.setSelectedIndex(2);
        }
        else
        {
            comboStaffType.setSelectedIndex(3);
        }
        
        
        
        txtPhoneNumber.setText(model.getValueAt(i,4).toString()); 
        txtBankAccount.setText(model.getValueAt(i,5).toString()); 
        txtSalaryRate.setText(model.getValueAt(i,6).toString()); 
        txtEmailUsername.setText(model.getValueAt(i,7).toString());
        txtPassword.setText(model.getValueAt(i,8).toString());
    }//GEN-LAST:event_tblShowPersonsMouseClicked

    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed
        boolean fieldIsEmpty = MissingFields();

            
        if(fieldIsEmpty == false)
        {
            String salt = makeSalt();
            String password = "";
            try {
                password = hashPassword(salt);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
            }
           JOptionPane.showMessageDialog(null,salt);
            Connection conn = JavaConnectDb.ConnectDb();
            try
            {
            //Use prepared statements to prevent SQL injections
                PreparedStatement ps = conn.prepareStatement("INSERT INTO STAFF (STAFF_NAME,STAFF_SURNAME,STAFF_TYPE,STAFF_CONTACT_NO,STAFF_BANK_ACCOUNT,SALARY_RATE,EMAIL_USERNAME,PASSWORD,SALT) VALUES(?,?,?,?,?,?,?,?,?)");
                ps.setString(1, txtFirstName.getText());
                ps.setString(2,txtLastName.getText());
                ps.setString(3,comboStaffType.getSelectedItem().toString());
                ps.setString(4,txtPhoneNumber.getText());
                ps.setString(5,txtBankAccount.getText());
                ps.setString(6,txtSalaryRate.getText());        
                ps.setString(7,txtEmailUsername.getText());        
                ps.setString(8,password);   
                ps.setString(9,salt);

                ps.executeUpdate();
                ps.close();
            }
            catch(SQLException ex)
            {
                Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
            
            JOptionPane.showMessageDialog(null,"New Staff succesfully inserted");
            ClearTextFields();
            DefaultTableModel model = (DefaultTableModel) tblShowPersons.getModel();
            model.setRowCount(0);
            ShowPersonsInTable();
        }
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        //There is no need to update the ID.
        String value = (String)comboStaffType.getSelectedItem();
        String comboValue = comboStaffType.getSelectedItem().toString();
         String salt = "";
        
        boolean fieldIsEmpty = MissingFields();
        Statement stmt = null;
        
        if(fieldIsEmpty == false)
        {
            int yesOrNo;
            yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to make these changes?","UPDATE STAFF",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
           
            if(yesOrNo == 0)
            {                                         
                Connection conn = JavaConnectDb.ConnectDb();
                try{
                stmt = conn.createStatement();

                
                String sql = "SELECT SALT FROM STAFF WHERE STAFF_ID = " + txtId.getText();
                    ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                     salt = rs.getString("SALT");
                }
                JOptionPane.showMessageDialog(null,salt);
                String password = hashPassword(salt);
                
                
                    //Use prepared statements to prevent SQL injections
                    PreparedStatement ps = conn.prepareStatement("UPDATE STAFF SET STAFF_NAME= ?, STAFF_SURNAME= ?, STAFF_TYPE= ?, STAFF_CONTACT_NO= ?, STAFF_BANK_ACCOUNT= ?, SALARY_RATE= ?, EMAIL_USERNAME= ?, PASSWORD= ?  WHERE STAFF_ID= ? ");
                    ps.setString(1, txtFirstName.getText());
                    ps.setString(2,txtLastName.getText());
                    ps.setString(3,comboValue);
                    ps.setString(4,txtPhoneNumber.getText());
                    ps.setString(5,txtBankAccount.getText());
                    ps.setString(6,txtSalaryRate.getText());        
                    ps.setString(7,txtEmailUsername.getText());        
                    ps.setString(8,password);        
                    ps.setString(9,txtId.getText());          

                    ps.executeUpdate();
                    ps.close();
                } catch (SQLException ex) 
                {
                    Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null,"SQL not working");
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
                }
                    JOptionPane.showMessageDialog(null,"Existing Staff succesfully updated");
                    ClearTextFields();
                    DefaultTableModel model = (DefaultTableModel) tblShowPersons.getModel();
                    model.setRowCount(0);
                    ShowPersonsInTable();
            }
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to delete this staff?","DELETE STAFF",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
        if(yesOrNo == 0)
        {   
            String query = "DELETE FROM STAFF WHERE STAFF_ID = "+txtId.getText(); 
            executeSqlQuery(query, "Staff Deleted");
            ClearTextFields();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogOutActionPerformed
        int yesNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to log out?","LOG OUT",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
                
        if(yesNo == 0) 
        {
            this.hide();
            Login login = new Login();
            login.setVisible(true);
        }
    }//GEN-LAST:event_btnLogOutActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ClearTextFields();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cmdSearchStaffTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdSearchStaffTypeActionPerformed
        SearchStaffTypeInTable();
    }//GEN-LAST:event_cmdSearchStaffTypeActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SearchByStaffName();
    }//GEN-LAST:event_btnSearchActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditStaffPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditStaffPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditStaffPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditStaffPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditStaffPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnInsert;
    private javax.swing.JButton btnLogOut;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cmdSearchStaffType;
    private javax.swing.JComboBox<String> comboStaffType;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblShowPersons;
    private javax.swing.JTextField txtBankAccount;
    private javax.swing.JTextField txtEmailUsername;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JTextField txtPassword;
    private javax.swing.JTextField txtPhoneNumber;
    private javax.swing.JTextField txtSalaryRate;
    private javax.swing.JTextField txtSearchName;
    // End of variables declaration//GEN-END:variables
}
