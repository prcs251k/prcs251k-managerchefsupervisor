/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagerGUI;

import DatabaseConnection.JavaConnectDb;
import ManagerGUI.ManagerMainPage;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import restaurant.staffdatamodel.Ingredient;
import restaurant.staffdatamodel.Item;
import restaurant.staffdatamodel.ItemSize;



/**
 *
 * @author Panagiotis
 */
public class EditMenu extends javax.swing.JFrame 
{

    //Store the txtFields of the gui in 2 arrays
    private JTextField[] arrayTextFieldsItem = new JTextField[2];
    private JTextField[] arrayTextFieldsItemSize = new JTextField[4];
    
    
    /**
     * Creates new form EditMenu
     */
    public EditMenu() 
    {
        initComponents();
        ShowItemsInTable();
        ShowItemsSizeInTable();
        tblShowMenuItems.setRowHeight(30);
        tableShowItemSizeItems.setRowHeight(30);
        Toolkit tk = Toolkit.getDefaultToolkit();
        int xsize = (int) tk.getScreenSize().getWidth();
        int ysize = (int) tk.getScreenSize().getHeight();
        this.setSize(xsize, ysize);
        this.setExtendedState(this.MAXIMIZED_BOTH);
        
        
        //Initialise the arrayTextFieldsItem
        arrayTextFieldsItem[0] = txtItemId;
        arrayTextFieldsItem[0].setText("AUTOCOMPLETE");
        arrayTextFieldsItem[1] = txtItemName;
        
        ////Initialise the arrayTextFieldsItemSize
        arrayTextFieldsItemSize[0] = txtItemId2;
        arrayTextFieldsItemSize[1] = txtItemSizeId;
        arrayTextFieldsItemSize[2] = txtItemName2;
        arrayTextFieldsItemSize[3] = txtSizePrice;
    }

    
    /**
     * Gets data from table Item in database.
     * @return an ArrayList containing objects of class Item.
     */
    public ArrayList<Item> getItemList()
    {
        ArrayList<Item> itemList = new ArrayList<Item>();
        
        Connection conn = JavaConnectDb.ConnectDb();
        
        String query = "SELECT ITEM_ID, ITEM_NAME, ITEM_TYPE FROM ITEM"; //At the end will be SELECT * FROM ITEM
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            Item item;
            while(rs.next())
            {
                item = new Item(rs.getInt("ITEM_ID"),rs.getString("ITEM_NAME"),rs.getString("ITEM_TYPE"));  //EDW KATI PAIZEI ME TO ID MALLON!!
                itemList.add(item);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return itemList;
    }
    
    
    /**
     * Gets data from table ItemSize in database.
     * @return an ArrayList containing objects of class ItemSize.
     */
    public ArrayList<ItemSize> getItemSizeList()
    {
        ArrayList<ItemSize> itemSizeList = new ArrayList<ItemSize>();
       
        Connection conn = JavaConnectDb.ConnectDb();
        
        String query = "SELECT SIZE_ID, UNIT_PRICE, ITEM_NAME, ITEM_ID,ITEM_TYPE  FROM ITEM_SIZE"; //At the end will be SELECT * FROM ITEM
        Statement st;
        ResultSet rs;
        
        try
        {
            st = conn.createStatement();
            rs = st.executeQuery(query);
            ItemSize itemSize;
            while(rs.next())
            {
                itemSize = new ItemSize(rs.getInt("SIZE_ID"),rs.getInt("UNIT_PRICE"),rs.getString("ITEM_NAME"),rs.getInt("ITEM_ID"),rs.getString("ITEM_TYPE"));
                itemSizeList.add(itemSize);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        
        return itemSizeList;
    }
    
    
    
    /**
     * Display data in table tblShowMenuItems
     */
    public void ShowItemsInTable()
    {
        ArrayList<Item> list = getItemList();
        DefaultTableModel model = (DefaultTableModel)tblShowMenuItems.getModel();
        Object[] row = new Object[3]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
        
        for(int i = 0; i < list.size(); i++)
        {  
                 
            row[0] = list.get(i).getItemId();
            row[1] = list.get(i).getItemName();
            row[2] = list.get(i).getItemType();  
            
            model.addRow(row);
            
        }    
       
    }
    
    
    
     
    
    /**
     * Display data in table tableShowItemSizeItems
     */
     public void ShowItemsSizeInTable()
    {
        ArrayList<ItemSize> itemSizeList = getItemSizeList();
        DefaultTableModel model2 = (DefaultTableModel)tableShowItemSizeItems.getModel();
        Object[] row = new Object[5]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
        
        for(int i = 0; i < itemSizeList.size(); i++)
        {
            row[0] = itemSizeList.get(i).getItemId();
            row[1] = itemSizeList.get(i).getItemName();
            row[2] = itemSizeList.get(i).getItemSizeId();
            row[3] = itemSizeList.get(i).getItemSizeType();
            row[4] = itemSizeList.get(i).getItemSizeUnitPrice();
            
            
            model2.addRow(row);
        }      
    }
    
     
    /**
     * Searches for Items based on their Types.
     */ 
    public void SearchItemTypeInTable()
    {
        String SearchText=cmbItemType.getSelectedItem().toString(); //txtSearch.getText();
        ArrayList<Item> list = getItemList();
        DefaultTableModel model = (DefaultTableModel)tblShowMenuItems.getModel();
         model.setRowCount(0);
        Object[] row = new Object[3]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
       
        boolean exist = false;
        for(int i = 0; i < list.size(); i++)
        {  if(SearchText.equalsIgnoreCase(list.get(i).getItemType()))
            {
                exist = true; 
            row[0] = list.get(i).getItemId();
            row[1] = list.get(i).getItemName();
            row[2] = list.get(i).getItemType();  
            
            model.addRow(row);
             
            } 
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
      
    
    /**
     * Searches for ItemSize objects based on their Types.
     */ 
      public void SearchItemSizeTypeInTable()
    {
        String SearchText=cmbItemSizeType.getSelectedItem().toString(); //txtSearch.getText();
        ArrayList<ItemSize> listSize = getItemSizeList();
        DefaultTableModel model = (DefaultTableModel)tableShowItemSizeItems.getModel();
        model.setRowCount(0);
        Object[] row = new Object[5]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
       
        boolean exist = false;
        for(int i = 0; i < listSize.size(); i++)
        {  if(SearchText.equalsIgnoreCase(listSize.get(i).getItemSizeType()))
            {
                
                exist = true; 
                row[0] = listSize.get(i).getItemId();
                row[1] = listSize.get(i).getItemName();
                row[2] = listSize.get(i).getItemSizeId();
                row[3] = listSize.get(i).getItemSizeType();
                row[4] = listSize.get(i).getItemSizeUnitPrice();

                model.addRow(row);

            } 
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
    
    /**
     * Searches for an Item object based on name
     */ 
    public void SearchItemInTable()
    {
        String SearchText=txtSearch.getText();
        ArrayList<Item> list = getItemList();
        DefaultTableModel model = (DefaultTableModel)tblShowMenuItems.getModel();
        model.setRowCount(0);
        Object[] row = new Object[3]; 
       
        boolean exist = false;
        for(int i = 0; i < list.size(); i++)
        {  if(SearchText.equalsIgnoreCase(list.get(i).getItemName()))
            {
                exist = true; 
            row[0] = list.get(i).getItemId();
            row[1] = list.get(i).getItemName();
            row[2] = list.get(i).getItemType();  
            
            model.addRow(row);
             
            } 
        
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }
      
    /**
     * Searches for an ItemSize object based on name
     */
      public void SearchItemSizeInTable()
    {
        String SearchText=txtSearchSize.getText();
        ArrayList<ItemSize> itemSizeList = getItemSizeList();
        DefaultTableModel model = (DefaultTableModel)tableShowItemSizeItems.getModel();
        model.setRowCount(0);
        Object[] row = new Object[5]; //4 Because using only the ID,ItemName,ItemPrice,ItemType constructor for now.
       
        boolean exist = false;
        for(int i = 0; i < itemSizeList.size(); i++)
        {  if(SearchText.equalsIgnoreCase(itemSizeList.get(i).getItemName()))
            {
                exist = true; 
           
            
            row[0] = itemSizeList.get(i).getItemId();
            row[1] = itemSizeList.get(i).getItemName();
            row[2] = itemSizeList.get(i).getItemSizeId();
            row[3] = itemSizeList.get(i).getItemSizeType();
            row[4] = itemSizeList.get(i).getItemSizeUnitPrice();
            
            model.addRow(row);
             
            } 
        
        
        }    
        if(exist == false)
        {
            JOptionPane.showMessageDialog(rootPane, "No matches were found. \nCheck your input");
        }
    }

    
    
    
    /**
     * Clear the textFields for Item
     */
    public void ClearTextFields()
    {
        txtItemId.setText("AUTOCOMPLETE");
        txtItemName.setText(""); 
    }
    
    
    /**
     * Clear the textFields for ItemSize
     */
    public void ClearTextSizeFields()
    {
        txtItemId2.setText("");
        txtItemName2.setText("");
        txtItemSizeId.setText("");
        txtSizePrice.setText("");    
    }
    
   
    
     /**
     * Check if all fields on Item are filled when editing a item.
     * @return true if there is an empty text field and false if not.
     */
    public boolean MissingFieldsOnItem()
    {
        boolean isMissing = false;
        
        for(int i = 0; i < arrayTextFieldsItem.length; i++)
        {    
            if(arrayTextFieldsItem[i].getText().equals(""))
            {
                isMissing = true;
                JOptionPane.showMessageDialog(rootPane, "All fields must be filled.");
                break;
            }
        } 
        return isMissing;
    }
    
    
    /**
     * Check if all fields on ItemSize are filled when editing a itemSize.
     * @return true if there is an empty text field and false if not.
     */
    public boolean MissingFieldsOnItemSize()
    {
        boolean isMissing = false;
        
        for(int i = 0; i < arrayTextFieldsItemSize.length; i++)
        {    
            if(arrayTextFieldsItemSize[i].getText().equals(""))
            {
                isMissing = true;
                JOptionPane.showMessageDialog(rootPane, "All fields must be filled.");
                break;
            }
        } 
        return isMissing;
    }
    
    
    
    

    
    
    
   
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblShowMenuItems = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        txtItemId = new javax.swing.JTextField();
        txtItemName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnInsert = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnClear2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableShowItemSizeItems = new javax.swing.JTable();
        txtItemId2 = new javax.swing.JTextField();
        ItemName2 = new javax.swing.JLabel();
        it = new javax.swing.JLabel();
        itemId4 = new javax.swing.JLabel();
        itemId5 = new javax.swing.JLabel();
        txtSizePrice = new javax.swing.JTextField();
        txtItemName2 = new javax.swing.JTextField();
        btnInsertSize = new javax.swing.JButton();
        btnUpdateSize = new javax.swing.JButton();
        btnDeleteSize = new javax.swing.JButton();
        jComboBoxTypeSize = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        it1 = new javax.swing.JLabel();
        txtItemSizeId = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        btnClear1 = new javax.swing.JButton();
        searchBtn = new javax.swing.JButton();
        clearSearchBtn = new javax.swing.JButton();
        cmbItemType = new javax.swing.JComboBox<>();
        cmbItemType2 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtSearchSize = new javax.swing.JTextField();
        searchSizeBtn = new javax.swing.JButton();
        clearSearchSizeBtn = new javax.swing.JButton();
        cmbItemSizeType = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Edit Menu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 0, 24), new java.awt.Color(255, 0, 0))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1920, 1080));

        tblShowMenuItems.setBackground(new java.awt.Color(51, 51, 51));
        tblShowMenuItems.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tblShowMenuItems.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblShowMenuItems.setForeground(new java.awt.Color(240, 240, 240));
        tblShowMenuItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item ID", "Item Name", "Item Type", "Item Picture"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblShowMenuItems.getTableHeader().setReorderingAllowed(false);
        tblShowMenuItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblShowMenuItemsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblShowMenuItems);
        if (tblShowMenuItems.getColumnModel().getColumnCount() > 0) {
            tblShowMenuItems.getColumnModel().getColumn(0).setResizable(false);
            tblShowMenuItems.getColumnModel().getColumn(1).setResizable(false);
            tblShowMenuItems.getColumnModel().getColumn(2).setResizable(false);
            tblShowMenuItems.getColumnModel().getColumn(3).setResizable(false);
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(240, 240, 240));
        jLabel1.setText("Search Item Name: ");

        txtSearch.setBackground(new java.awt.Color(51, 51, 51));
        txtSearch.setForeground(new java.awt.Color(240, 240, 240));
        txtSearch.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtItemId.setEditable(false);
        txtItemId.setBackground(new java.awt.Color(51, 51, 51));
        txtItemId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtItemId.setForeground(new java.awt.Color(240, 240, 240));
        txtItemId.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtItemName.setBackground(new java.awt.Color(51, 51, 51));
        txtItemName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtItemName.setForeground(new java.awt.Color(240, 240, 240));
        txtItemName.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(240, 240, 240));
        jLabel2.setText("Item ID:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(240, 240, 240));
        jLabel3.setText("Name:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(240, 240, 240));
        jLabel6.setText("Type: ");

        btnInsert.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnInsert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Plus_20.png"))); // NOI18N
        btnInsert.setText("Insert");
        btnInsert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Update_20.png"))); // NOI18N
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Delete_20.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnClear2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClear2.setText("Clear Text");
        btnClear2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));
        btnClear2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear2ActionPerformed(evt);
            }
        });

        tableShowItemSizeItems.setBackground(new java.awt.Color(51, 51, 51));
        tableShowItemSizeItems.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tableShowItemSizeItems.setForeground(new java.awt.Color(255, 255, 255));
        tableShowItemSizeItems.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item ID", "Item Name", "Size ID", "Size Type", "Size Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableShowItemSizeItems.getTableHeader().setReorderingAllowed(false);
        tableShowItemSizeItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableShowItemSizeItemsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableShowItemSizeItems);
        if (tableShowItemSizeItems.getColumnModel().getColumnCount() > 0) {
            tableShowItemSizeItems.getColumnModel().getColumn(0).setResizable(false);
            tableShowItemSizeItems.getColumnModel().getColumn(1).setResizable(false);
            tableShowItemSizeItems.getColumnModel().getColumn(2).setResizable(false);
            tableShowItemSizeItems.getColumnModel().getColumn(3).setResizable(false);
            tableShowItemSizeItems.getColumnModel().getColumn(4).setResizable(false);
        }

        txtItemId2.setEditable(false);
        txtItemId2.setBackground(new java.awt.Color(51, 51, 51));
        txtItemId2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtItemId2.setForeground(new java.awt.Color(240, 240, 240));
        txtItemId2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        ItemName2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        ItemName2.setForeground(new java.awt.Color(255, 255, 255));
        ItemName2.setText("Item Name:");

        it.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        it.setForeground(new java.awt.Color(255, 255, 255));
        it.setText("Item ID:");

        itemId4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        itemId4.setForeground(new java.awt.Color(255, 255, 255));
        itemId4.setText("Size Type:");

        itemId5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        itemId5.setForeground(new java.awt.Color(255, 255, 255));
        itemId5.setText("Size Price");

        txtSizePrice.setBackground(new java.awt.Color(51, 51, 51));
        txtSizePrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtSizePrice.setForeground(new java.awt.Color(240, 240, 240));
        txtSizePrice.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        txtItemName2.setBackground(new java.awt.Color(51, 51, 51));
        txtItemName2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtItemName2.setForeground(new java.awt.Color(240, 240, 240));
        txtItemName2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        btnInsertSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnInsertSize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Plus_20.png"))); // NOI18N
        btnInsertSize.setText("Insert");
        btnInsertSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInsertSizeActionPerformed(evt);
            }
        });

        btnUpdateSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUpdateSize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Update_20.png"))); // NOI18N
        btnUpdateSize.setText("Update");
        btnUpdateSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateSizeActionPerformed(evt);
            }
        });

        btnDeleteSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDeleteSize.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Delete_20.png"))); // NOI18N
        btnDeleteSize.setText("Delete");
        btnDeleteSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteSizeActionPerformed(evt);
            }
        });

        jComboBoxTypeSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jComboBoxTypeSize.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SMALL", "MEDIUM", "LARGE", "330ml", "500ml", "1.5L" }));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 0));
        jLabel7.setText("Item Size");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText("Item");

        it1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        it1.setForeground(new java.awt.Color(255, 255, 255));
        it1.setText("Item Size ID:");

        txtItemSizeId.setEditable(false);
        txtItemSizeId.setBackground(new java.awt.Color(51, 51, 51));
        txtItemSizeId.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtItemSizeId.setForeground(new java.awt.Color(240, 240, 240));
        txtItemSizeId.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("£");

        btnClear1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClear1.setText("Clear Text");
        btnClear1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));
        btnClear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClear1ActionPerformed(evt);
            }
        });

        searchBtn.setText("Search");
        searchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchBtnActionPerformed(evt);
            }
        });

        clearSearchBtn.setText("Clear");
        clearSearchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearSearchBtnActionPerformed(evt);
            }
        });

        cmbItemType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PIZZA", "SIDE", "DRINK", "DESERT", "DEAL" }));
        cmbItemType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbItemTypeActionPerformed(evt);
            }
        });

        cmbItemType2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cmbItemType2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PIZZA", "SIDE", "DRINK", "DESERT", "DEAL" }));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(240, 240, 240));
        jLabel4.setText("Search Item Name: ");

        txtSearchSize.setBackground(new java.awt.Color(51, 51, 51));
        txtSearchSize.setForeground(new java.awt.Color(240, 240, 240));
        txtSearchSize.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED, new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240), new java.awt.Color(240, 240, 240)));

        searchSizeBtn.setText("Search");
        searchSizeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchSizeBtnActionPerformed(evt);
            }
        });

        clearSearchSizeBtn.setText("Clear");
        clearSearchSizeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearSearchSizeBtnActionPerformed(evt);
            }
        });

        cmbItemSizeType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SMALL", "MEDIUM", "LARGE", "330ml", "500ml", "1.5L" }));
        cmbItemSizeType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbItemSizeTypeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(112, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(itemId4)
                            .addComponent(itemId5))
                        .addGap(443, 443, 443))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(62, 62, 62)
                                .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(112, 112, 112)
                                .addComponent(btnInsertSize, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jComboBoxTypeSize, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(txtSizePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(84, 84, 84)
                                                .addComponent(btnClear1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(btnUpdateSize, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(53, 53, 53)
                                        .addComponent(btnDeleteSize, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(60, 60, 60)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(it1)
                                        .addComponent(it)
                                        .addComponent(ItemName2))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 53, Short.MAX_VALUE)
                                    .addComponent(txtItemName2, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(122, 122, 122)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel2)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(jLabel6)
                                                    .addComponent(jLabel3)))
                                            .addGap(56, 56, 56)
                                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(txtItemId, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                                                .addComponent(txtItemName)
                                                .addComponent(cmbItemType2, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(btnDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                                            .addComponent(btnClear2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addComponent(txtItemId2, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtItemSizeId, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(searchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(clearSearchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67)
                        .addComponent(cmbItemType, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1154, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(90, 90, 90))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 543, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(txtSearchSize, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(searchSizeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(clearSearchSizeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67)
                .addComponent(cmbItemSizeType, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(456, 456, 456))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(jLabel8)
                    .addContainerGap(1826, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(cmbItemType, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(67, 67, 67)
                            .addComponent(jLabel1))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(searchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(clearSearchBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txtSearch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtItemId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txtItemName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(cmbItemType2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addComponent(btnClear2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnDelete)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbItemSizeType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(20, 20, 20)
                                    .addComponent(jLabel4))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(searchSizeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(clearSearchSizeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(txtSearchSize, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(29, 29, 29)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtItemId2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(it))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtItemSizeId, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(it1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(ItemName2)
                                    .addComponent(txtItemName2))
                                .addGap(38, 38, 38)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(itemId4)
                                    .addComponent(jComboBoxTypeSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(itemId5)
                                    .addComponent(txtSizePrice, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnClear1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(34, 34, 34)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnInsertSize, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnUpdateSize, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnDeleteSize))
                                .addGap(13, 13, 13))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 16, Short.MAX_VALUE)))))
                .addComponent(btnBack)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(33, 33, 33)
                    .addComponent(jLabel8)
                    .addContainerGap(891, Short.MAX_VALUE)))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtItemId, txtItemName});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1932, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1010, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tblShowMenuItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblShowMenuItemsMouseClicked
        // Display Selected Row in TextFields
        
        int i = tblShowMenuItems.getSelectedRow();
        TableModel model = tblShowMenuItems.getModel();
        
        txtItemId.setText(model.getValueAt(i,0).toString());
        txtItemName.setText(model.getValueAt(i,1).toString());
        cmbItemType2.setSelectedItem(model.getValueAt(i,2).toString());
        txtItemId2.setText(model.getValueAt(i,0).toString());
        txtItemName2.setText(model.getValueAt(i,1).toString());
        txtItemSizeId.setText("");
        
        
        //Have to complete the code for the picture as well.
    }//GEN-LAST:event_tblShowMenuItemsMouseClicked

    private void btnInsertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertActionPerformed

        boolean fieldIsEmpty = MissingFieldsOnItem();
        String type = cmbItemType2.getSelectedItem().toString();
        
        
        if(fieldIsEmpty == false)
        {       
            Connection conn = JavaConnectDb.ConnectDb();
            try
            {
                //Use prepared statements to prevent SQL injections
                PreparedStatement ps = conn.prepareStatement("INSERT INTO ITEM (ITEM_NAME,ITEM_TYPE) VALUES(?,?)");
                ps.setString(1,txtItemName.getText());
                ps.setString(2,type);              

                ps.executeUpdate();
                ps.close();
                
                JOptionPane.showMessageDialog(null,"New Item succesfully inserted");
                ClearTextFields();
                DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                model.setRowCount(0);
                model2.setRowCount(0);
                ShowItemsInTable();
                ShowItemsSizeInTable();
                ClearTextFields();
                ClearTextSizeFields();
            }
            catch(SQLException ex)
            {
                Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
            }     
        }
    }//GEN-LAST:event_btnInsertActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        //There is no need to update the ID.
        String type = cmbItemType2.getSelectedItem().toString();
        
        boolean fieldIsEmpty = MissingFieldsOnItem();
        
        if(fieldIsEmpty == false)
        {
            int yesOrNo;
            yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to make these changes?","UPDATE STAFF",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
           
            if(yesOrNo == 0)
            { 
                    Connection conn = JavaConnectDb.ConnectDb();
                    try 
                    {
                        //Use prepared statements to prevent SQL injections
                        PreparedStatement ps = conn.prepareStatement("UPDATE ITEM SET ITEM_NAME= ?, ITEM_TYPE= ?  WHERE ITEM_ID= ? ");
                        ps.setString(1, txtItemName.getText());
                        ps.setString(2,type);
                        ps.setString(3,txtItemId.getText());

                        ps.executeUpdate();
                        ps.close();
                        
                        DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                        DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                        model.setRowCount(0);
                        model2.setRowCount(0);
                        ShowItemsInTable();
                        ShowItemsSizeInTable(); 
                        ClearTextFields();
                        ClearTextSizeFields();
                    } catch (SQLException ex) 
                    {
                        Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
        
            
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to delete this item?","DELETE ITEM",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
        if(yesOrNo == 0)
        {        
            Connection conn = JavaConnectDb.ConnectDb();
            try 
            {
                PreparedStatement ps = conn.prepareStatement("DELETE FROM ITEM WHERE ITEM_ID = ?");
                ps.setString(1,txtItemId.getText());
                
                ps.executeUpdate();
                ps.close();
                
                       
                DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                model.setRowCount(0);
                model2.setRowCount(0);
                ShowItemsInTable();
                ShowItemsSizeInTable(); 
                ClearTextFields();
                ClearTextSizeFields();
            } catch (SQLException ex) {
                Logger.getLogger(EditMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.hide();
        ManagerMainPage mmp = new ManagerMainPage();
        mmp.setVisible(true);     
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnClear2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear2ActionPerformed
        ClearTextFields();
    }//GEN-LAST:event_btnClear2ActionPerformed

    private void btnInsertSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInsertSizeActionPerformed
        String type = jComboBoxTypeSize.getSelectedItem().toString();
          
        boolean fieldIsEmpty = MissingFieldsOnItem();
        
        if(fieldIsEmpty == false)
        {  
            Connection conn = JavaConnectDb.ConnectDb();
            try
            {
                //Use prepared statements to prevent SQL injections
                PreparedStatement ps = conn.prepareStatement("INSERT INTO ITEM_SIZE(ITEM_NAME,UNIT_PRICE,ITEM_TYPE) VALUES(?,?,?)");
                ps.setString(1,txtItemName2.getText());
                ps.setString(2,txtSizePrice.getText());        
                ps.setString(3,type); 

                ps.executeUpdate();
                ps.close();
                
                DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                model.setRowCount(0);
                model2.setRowCount(0);
                ShowItemsInTable();
                ShowItemsSizeInTable();
                ClearTextFields();
                ClearTextSizeFields();
            }
            catch(SQLException ex)
            {
                Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
            }

            
        } 
    }//GEN-LAST:event_btnInsertSizeActionPerformed

    private void btnUpdateSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateSizeActionPerformed
        // TODO add your handling code here:
        String type = jComboBoxTypeSize.getSelectedItem().toString();
        boolean fieldIsEmpty = MissingFieldsOnItemSize();

        
        if(fieldIsEmpty == false)
        {
            int yesOrNo;
            yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to make these changes?","UPDATE STAFF",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
           
            if(yesOrNo == 0)
            {   
                Connection conn = JavaConnectDb.ConnectDb();
                try 
                {
                    //Use prepared statements to prevent SQL injections
                    PreparedStatement ps = conn.prepareStatement("UPDATE ITEM_SIZE SET ITEM_NAME= ?, ITEM_TYPE= ?, UNIT_PRICE= ?  WHERE SIZE_ID= ? ");
                    ps.setString(1, txtItemName2.getText());
                    ps.setString(2,type);
                    ps.setString(3,txtSizePrice.getText());
                    ps.setString(4,txtItemSizeId.getText());   

                    ps.executeUpdate();
                    ps.close();
                    
                    DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                    DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                    model.setRowCount(0);
                    model2.setRowCount(0);
                    ShowItemsInTable();
                    ShowItemsSizeInTable(); 
                    ClearTextFields();
                    ClearTextSizeFields();
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(EditStaffPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
    }//GEN-LAST:event_btnUpdateSizeActionPerformed

    private void btnDeleteSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteSizeActionPerformed
         String query = "DELETE FROM ITEM_SIZE WHERE SIZE_ID = "+txtItemSizeId.getText();
        
        
        int yesOrNo;
        yesOrNo = JOptionPane.showConfirmDialog(rootPane,"Are you sure you want to delete this item?","DELETE ITEM",JOptionPane.ERROR_MESSAGE,JOptionPane.YES_NO_OPTION);
        if(yesOrNo == 0)
        {        
            Connection conn = JavaConnectDb.ConnectDb();
            try 
            {
                PreparedStatement ps = conn.prepareStatement("DELETE FROM ITEM_SIZE WHERE SIZE_ID = ?");
                ps.setString(1,txtItemSizeId.getText());
                
                ps.executeUpdate();
                ps.close();
                
                       
                DefaultTableModel model = (DefaultTableModel) tblShowMenuItems.getModel();
                DefaultTableModel model2 = (DefaultTableModel) tableShowItemSizeItems.getModel();
                model.setRowCount(0);
                model2.setRowCount(0);
                ShowItemsInTable();
                ShowItemsSizeInTable(); 
                ClearTextFields();
                ClearTextSizeFields();
            } catch (SQLException ex) {
                Logger.getLogger(EditMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnDeleteSizeActionPerformed

    private void tableShowItemSizeItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableShowItemSizeItemsMouseClicked
        // TODO add your handling code here:
        
        int i = tableShowItemSizeItems.getSelectedRow();
        TableModel model2 = tableShowItemSizeItems.getModel();
        
        txtItemSizeId.setText((model2.getValueAt(i,2).toString()));
        jComboBoxTypeSize.setSelectedItem((model2.getValueAt(i,3).toString()));
        txtItemId2.setText(model2.getValueAt(i,0).toString());
        txtItemName2.setText(model2.getValueAt(i,1).toString());
        txtSizePrice.setText(model2.getValueAt(i,4).toString());
    }//GEN-LAST:event_tableShowItemSizeItemsMouseClicked

    private void btnClear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClear1ActionPerformed
        // TODO add your handling code here:
        
        ClearTextSizeFields();
        
    }//GEN-LAST:event_btnClear1ActionPerformed

    private void searchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchBtnActionPerformed
        // TODO add your handling code here:
        SearchItemInTable();
    }//GEN-LAST:event_searchBtnActionPerformed

    private void clearSearchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearSearchBtnActionPerformed
        txtSearch.setText(" ");
        ShowItemsInTable();
    }//GEN-LAST:event_clearSearchBtnActionPerformed

    private void cmbItemTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbItemTypeActionPerformed
        // TODO add your handling code here:
        SearchItemTypeInTable();
    }//GEN-LAST:event_cmbItemTypeActionPerformed

    private void searchSizeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchSizeBtnActionPerformed
        // TODO add your handling code here:
        
        SearchItemSizeInTable();
    }//GEN-LAST:event_searchSizeBtnActionPerformed

    private void clearSearchSizeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearSearchSizeBtnActionPerformed
        txtSearchSize.setText(" ");
        ShowItemsSizeInTable();
    }//GEN-LAST:event_clearSearchSizeBtnActionPerformed

    private void cmbItemSizeTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbItemSizeTypeActionPerformed
        // TODO add your handling code here:
        SearchItemSizeTypeInTable();
    }//GEN-LAST:event_cmbItemSizeTypeActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ItemName2;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnClear1;
    private javax.swing.JButton btnClear2;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDeleteSize;
    private javax.swing.JButton btnInsert;
    private javax.swing.JButton btnInsertSize;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdateSize;
    private javax.swing.JButton clearSearchBtn;
    private javax.swing.JButton clearSearchSizeBtn;
    private javax.swing.JComboBox<String> cmbItemSizeType;
    private javax.swing.JComboBox<String> cmbItemType;
    private javax.swing.JComboBox<String> cmbItemType2;
    private javax.swing.JLabel it;
    private javax.swing.JLabel it1;
    private javax.swing.JLabel itemId4;
    private javax.swing.JLabel itemId5;
    private javax.swing.JComboBox<String> jComboBoxTypeSize;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton searchBtn;
    private javax.swing.JButton searchSizeBtn;
    private javax.swing.JTable tableShowItemSizeItems;
    private javax.swing.JTable tblShowMenuItems;
    private javax.swing.JTextField txtItemId;
    private javax.swing.JTextField txtItemId2;
    private javax.swing.JTextField txtItemName;
    private javax.swing.JTextField txtItemName2;
    private javax.swing.JTextField txtItemSizeId;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSearchSize;
    private javax.swing.JTextField txtSizePrice;
    // End of variables declaration//GEN-END:variables
}
